import React from 'react'
import styles from './styles/integration.module.scss'
import Image from 'next/image'
import WorkSpace from '../../assets/images/workspace.png'
import FaceBook from '../../assets/images/facebook.png'
import MailChimp from '../../assets/images/mailchimp.jpeg'
import Microsoft from '../../assets/images/microsoft.png'
import Shopify from '../../assets/images/shopify.png'
import Slack from '../../assets/images/slack.jpg'
import Pandadoc from '../../assets/images/pandadoc.png'
import ZohoSocial from '../../assets/images/zoho-social.png'
import ZohoSheet from '../../assets/images/zoho-sheet.png'
import ZohoSign from '../../assets/images/zoho-sign.png'
import ZohoWorkdrive from '../../assets/images/zoho-workdrive.jpeg'
import ZohoSalesiq from '../../assets/images/zoho-salesiq.png'
import WhatsApp from '../../assets/images/whats-app.png'
import { Metadata } from 'next'



export const metadata :Metadata= {
    title: 'Powerful Integrations for Zoho CRM: Amplify Your Business with WEST ADVISORY',
    description: 'Discover unique and powerful integrations for Zoho CRM. Harness the full potential of Zoho CRM with robust integrations from Whatsapp, Zoho Sheet, Zoho WorkDrive, Zoho SalesIQ, to Zoho Social, brought to you by WEST ADVISORY.',
    keywords: 'Zoho CRM, Whatsapp, Zoho Sheet, Zoho WorkDrive, Zoho SalesIQ, Zoho Social, integrations, WEST ADVISORY',
   
  };


const Integrantion = () => {

  return (
    <>
    <div className={styles.integration}>
      <div className={`${styles.imageBanner} item-center`}>
          <div className={styles.mainContentBanner}>
            <h1 className={styles.title} title='ZOHO CRM INTEGRATION'>Integrate your favorite apps into ZOHO CRM</h1>
            <p className={styles.benefitIntegration}>
            Spend less time managing software and more time managing your business. Zoho CRM is designed to integrate with other apps and services you use every day. Find tools you already use or discover new ways to improve your business.
            </p>
            <button className={styles.discover}>Discover</button>
          </div>
      </div>
      <div className={styles.outstanding}>
        <h2 className={styles.titleOutStanding}>Outstanding integration features</h2> 
        <div className={styles.mainOutStanding}>
            <div className={  `${styles.lineItem} item-btw`}>
                <div className={styles.item}>
                   <Image
                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                    src={WorkSpace}
                    placeholder="blur"
                    width={0}
                    height={0}
                    sizes="100vw"
                    style={{ width: '100%', height: 'auto' }}
                  /> 
                </div>
                <div className={styles.item}>
                  <Image
                      alt="WEST ADVISORY - FaceBook integrantion Zoho CRM"
                      src={FaceBook}
                      placeholder="blur"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }}
                    /> 
                </div>

            </div>
            <div className={  `${styles.lineItem} item-btw`}>
                <div className={styles.item}>
                   <Image
                    alt="WEST ADVISORY - MailChimp integrantion Zoho CRM"
                    src={MailChimp}
                    placeholder="blur"
                    width={0}
                    height={0}
                    sizes="100vw"
                    style={{ width: '100%', height: 'auto' }}
                  /> 
                </div>
                <div className={styles.item}>
                  <Image
                      alt="WEST ADVISORY - Microsoft integrantion Zoho CRM"
                      src={Microsoft}
                      placeholder="blur"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }}
                    /> 
                </div>

            </div>
            <div className={  `${styles.lineItem} item-btw`}>
                <div className={styles.item}>
                   <Image
                    alt="WEST ADVISORY - Shopify integrantion Zoho CRM"
                    src={Shopify}
                    placeholder="blur"
                    width={0}
                    height={0}
                    sizes="100vw"
                    style={{ width: '100%', height: 'auto' }}
                  /> 
                </div>
                <div className={styles.item}>
                  <Image
                      alt="WEST ADVISORY - Slack integrantion Zoho CRM"
                      src={Slack}
                      placeholder="blur"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }}
                    /> 
                </div>

            </div>
        </div>

      </div>
      <div className={styles.integrationFeature}>
        <h2 className={styles.title}>Our most popular integration feature</h2>
        <p className={styles.expansion}> 
          Choose from some of the best business apps for marketing, collaboration, phone, messaging, customer support, and more.
        </p>
        <div className={styles.officeSuite}>
            <h3 className={styles.titleProduct}>With Office Suite</h3>
            <div className={`${styles.listIntegration} `}>
                <div className={styles.itemIntegration}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                          src={WorkSpace}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>WorkSpace</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                <div className={`${styles.itemIntegration} ${styles.itemIntegrationCustom}`}>
                    <div className={styles.imageService}>
                    <Image
                      alt="WEST ADVISORY - Microsoft integrantion Zoho CRM"
                      src={Microsoft}
                      placeholder="blur"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }}
                    /> 
                    
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Microsoft</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                <div className={`${styles.itemIntegration} ${styles.itemIntegrationCustom}`}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Pandadoc integrantion Zoho CRM"
                          src={Pandadoc}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Pandadoc</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div className={styles.officeSuite}>
            <h3 className={styles.titleProduct}>With Manage Document</h3>
            <div className={`${styles.listIntegration} `}>
                <div className={styles.itemIntegration}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Zoho Sheet integrantion Zoho CRM"
                          src={ZohoSheet}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Zoho Sheet</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                <div className={`${styles.itemIntegration} ${styles.itemIntegrationCustom}`}>
                    <div className={styles.imageService}>
                    <Image
                      alt="WEST ADVISORY - Zoho Workdrive integrantion Zoho CRM"
                      src={ZohoWorkdrive}
                      placeholder="blur"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }}
                    /> 
                    
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Zoho Workdrive</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                <div className={`${styles.itemIntegration} ${styles.itemIntegrationCustom}`}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Pandadoc integrantion Zoho CRM"
                          src={Pandadoc}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Pandadoc</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div className={styles.officeSuite}>
            <h3 className={styles.titleProduct}>With Social</h3>
            <div className={`${styles.listIntegration} `}>
                <div className={styles.itemIntegration}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - FaceBook integrantion Zoho CRM"
                          src={FaceBook}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>FaceBook</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                <div className={`${styles.itemIntegration} ${styles.itemIntegrationCustom}`}>
                    <div className={styles.imageService}>
                    <Image
                      alt="WEST ADVISORY - Zoho Social integrantion Zoho CRM"
                      src={ZohoSocial}
                      placeholder="blur"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }}
                    /> 
                    
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Zoho Social</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div className={styles.officeSuite}>
            <h3 className={styles.titleProduct}>With Message</h3>
            <div className={`${styles.listIntegration} `}>
                <div className={styles.itemIntegration}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                          src={WhatsApp}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>WhatsApp</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
        <div className={styles.officeSuite}>
            <h3 className={styles.titleProduct}>With Sign Document</h3>
            <div className={`${styles.listIntegration} `}>
                <div className={styles.itemIntegration}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Zoho Sign integrantion Zoho CRM"
                          src={ZohoSign}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>ZohoSign</span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
        <div className={styles.officeSuite}>
            <h3 className={styles.titleProduct}>With Chatbox </h3>
            <div className={`${styles.listIntegration} `}>
                <div className={styles.itemIntegration}>
                    <div className={styles.imageService}>
                      <Image
                          alt="WEST ADVISORY - Zoho SalesIq integrantion Zoho CRM"
                          src={ZohoSalesiq}
                          placeholder="blur"
                          width={0}
                          height={0}
                          sizes="100vw"
                          style={{ width: '100%', height: 'auto' }}
                        /> 
                    </div>
                    <div className={styles.researchMore}>
                        <div className={`${styles.layoutResearchMore} item-btw w-100`}>
                              <span className={styles.titleMore}>Zoho Salesiq </span>
                              <button className={styles.btnMore}>More Details</button>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
      </div>
        
        
    </div>
    </>
    
  )
}

export default Integrantion