import React from 'react'
import Banner from './components/Banner/Banner'
import Benefit from './components/Benefit/Benefit'
import PostNew from './components/PostNew/PostNew'
import Service from './components/Service/Service'
import {Metadata} from 'next'
import ListTutorialDB from './components/PostNew/constants'
import Blogs from './components/Blogs/Blogs'


// either Static metadata
export const metadata: Metadata = {
    title: "HQL - Revit MEP Plugins | Plugin, Add-on, Extension for Revit",
    description: "Khám phá các plugin, add-on và extension HQL mạnh mẽ cho Revit, giúp tối ưu hóa quy trình MEP và nâng cao hiệu suất với Revit API."
}




const Bim = async () => {

  return (
    <div>
        <Banner/>
        <Service/>
        <Benefit/>  
        {/* <Tutorial/> */}
        <PostNew tutorials = {ListTutorialDB}/>
        <Blogs/>
    </div>
  )
}




export default Bim