"use client"
import styles from './styles/post-new.module.scss'
import "swiper/css"
import "swiper/css/free-mode"
import "swiper/css/pagination"
import { Swiper, SwiperSlide } from "swiper/react"
import { Autoplay, Pagination,Navigation } from "swiper"
import Image from 'next/image'
import { useRef } from 'react'
import Poster1 from '../../../../assets/images/poster1.webp'
import { ITutorial } from './constants'


interface Props {
    tutorials: ITutorial[];
}

const PostNew : React.FC<Props>= ({tutorials}:any) => {

    const navigationPrevRef = useRef<any>(null)
    const navigationNextRef = useRef<any>(null)
    return (
    <div className={styles.postNew}>
        <div className={styles.title}>
            <p>Bài viết mới nhất</p>
        </div>
        <div className={styles.posts}>
                <div className={styles.previous} ref={navigationPrevRef}><i className="fa-solid fa-chevron-left"/></div>
                <div className={styles.next} ref={navigationNextRef}><i className="fa-solid fa-chevron-right"/></div>
                <>
                    <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        autoplay={{
                          delay: 2500,
                          disableOnInteraction: false,
                        }}
                        pagination={{ 
                          clickable: true,
                        }}
                        modules={[Autoplay, Pagination, Navigation]}
                        className="mySwiper"
                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                          }}
                        onSwiper={(swiper:any) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current
                            swiper.params.navigation.nextEl = navigationNextRef.current
                            swiper.navigation.destroy()
                            swiper.navigation.init()
                            swiper.navigation.update()
                        }}
                    >
                         {tutorials.flatMap((value:any,index:number)=>{
                            return <SwiperSlide key={`postnew`+index}>
                                        <a href={value.url} target='_blank'>
                                            <div className={styles.posted}>
                                                <div className={styles.image}>
                                                <Image
                                                    alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                                                    src={Poster1}
                                                    placeholder="blur"
                                                    width={0}
                                                    height={0}
                                                    sizes="100vw"
                                                    style={{ width: '100%', height: 'auto' }}
                                                /> 
                                                </div>
                                                <div className={styles.mainPost}>
                                                    <p className={styles.title}>{value.title}</p>
                                                    <div></div>
                                                    <p  className={styles.description}>{value.description.length > 60 ? value.description.slice(0,60 ) : value.description +"..." }</p>
                                                    <button className={styles.read}>Xem hướng dẫn</button>
                                                </div>
                                            </div>  
                                        </a>    
                                    </SwiperSlide>
                         })}
                    </Swiper>
            </>     
        </div>
    </div>
  )
}

  
  

export default PostNew

