import React from 'react'
import styles from './styles/benefit.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faBusinessTime, faCreditCard, faHandshakeAngle, faHeadset, faSackDollar } from '@fortawesome/free-solid-svg-icons'
import { faCloudflare } from '@fortawesome/free-brands-svg-icons';
const Benefit = () => {
  return (

    <section className={`${styles.benefit} container`}>
        <h2 className={styles.title}>Các lợi ích chúng tôi mang đến cho bạn</h2>

        <div className={styles.note}>
            <p>Chúng tôi luôn cố gắng hết sức để mang đến cho Quý khách hàng những trải nghiệm tuyệt vời nhất có thể</p>
        </div>
        <div className={styles.line1}>
            <div className={styles.item}>
                <div className={styles.image}>
                    <span className={`${styles.icon}`}>  <FontAwesomeIcon icon={faBusinessTime} /></span>
                </div>
                <div className={styles.maiContent}>
                    <h3 className={styles.title}>Tiết kiệm thời gian</h3>
                    <p className={styles.content}>Bạn sẽ tiết kiệm được nhiều thời gian khi dùng Sản phẩm và dịch vụ của chúng tôi</p>
                </div>
            </div>

            <div className={styles.item}>
                <div className={styles.image}>
                     <span className={` ${styles.icon}`}> <FontAwesomeIcon icon={faSackDollar} /></span>
                </div>
                <div className={styles.maiContent}>
                    <h3 className={styles.title}>Chi Phí</h3>
                    <p className={styles.content}>Bạn có thể yên tâm sử dụng dịch vụ của Chúng tôi chỉ từ 90.000/tháng.</p>
                </div>
            </div>
            <div className={styles.item}>
                <div className={styles.image}>
                    {/* <i className="fa-sharp fa-solid fa-cloud-arrow-down"></i> */}
                    <span className={` ${styles.icon}`}> <FontAwesomeIcon icon={faCloudflare} /></span>
                </div>
                <div className={styles.maiContent}>
                    <h3 className={styles.title}>Cập nhật tự động</h3>
                    <p className={styles.content}>Các sản phẩm tự động cập nhật khi có kết nối Internet</p>
                </div>
            </div>
        </div>
        <div className={styles.line1}>
            <div className={styles.item}>
                <div className={styles.image}>

                <span className={`${styles.icon}`}> <FontAwesomeIcon icon={faCreditCard} /></span>
                </div>
                <div className={styles.maiContent}>
                    <h3 className={styles.title}>Thanh toán linh hoạt</h3>
                    <p className={styles.content}>Bạn chỉ cần ngồi tại chỗ và thanh toán các sản phẩm với rất nhiều tùy chọn thanh toán.</p>
                </div>
            </div>

            <div className={styles.item}>
                <div className={styles.image}>
                     <i className="fa-sharp fa-solid fa-handshake-angle"></i>
                     <span className={`${styles.icon}`}> <FontAwesomeIcon icon={faHandshakeAngle} /></span>
                </div>
                <div className={styles.maiContent}>
                    <h3 className={styles.title}>Đối tác tin cây</h3>
                    <p className={styles.content}>Nhiều chính sách hấp dân khi bạn trở thành nhà phân phối.</p>
                </div>
            </div>
            <div className={styles.item}>
                <div className={styles.image}>

                    <span className={`${styles.icon}`}> <FontAwesomeIcon icon={faHeadset} /></span>
                </div>
                <div className={styles.maiContent}>
                    <h3 className={styles.title}>Hỗ trợ 24/7</h3>
                    <p className={styles.content}>Chúng tôi luôn cố gắng để lắng nghe bạn mỗi khi bạn gặp khó khăn khi sử dụng dịch vụ.</p>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Benefit
