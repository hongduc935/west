"use client"
import { useState } from 'react'
// import Image from 'components/Image/Image'
import TrainingLogo from '../../../../assets/images/Logo_Bim.png'
import Training1 from '../../../../assets/images/TrainingAPI.png'
import Training2 from '../../../../assets/images/TrainingRevit.jpg'


import styles from './styles/service.module.scss'
import Image from 'next/image'
import MyPdfViewer from '../../../../components/PdfViewerComponent/PdfViewerComponent'
const content1:string ="Bộ công cụ tự động hóa mạnh mẽ trong Revit MEP, giúp nâng cao hiệu suất làm việc cho kỹ sư Bim. Các tính năng tiên tiến thay thế thao tác lặp đi lặp lại, giúp tiết kiệm thời gian và công sức."
const content2:string = "Giải pháp cho công ty Bim, giúp bạn chuyển đổi từ thiết kế thủ công sang mô hình 3D hiện đại. Tối ưu hóa thời gian, nhân lực và chi phí với các công cụ hiệu quả."
const content3:string = "Khoá học nâng cao cho kỹ sư Bim Mep, giúp bạn tạo sự khác biệt và đạt mức lương cao hơn. Học cách sử dụng API để tự động hóa và cải tiến quy trình làm việc của bạn."
const content4:string = "Khóa học thiết yếu cho kỹ sư Bim Mep theo chuẩn của thủ tướng chính phủ. Nắm vững công cụ Revit Mep để làm chủ ngành xây dựng Bim, đảm bảo bạn không bị bỏ lại phía sau."
const Service = () => {


    let [isMore1,setIsMore1] = useState(true)
    let [isMore2,setIsMore2] = useState(true)
    let [isMore3,setIsMore3] = useState(true)
    let [isMore4,setIsMore4] = useState(true)
 
    let [openPDF,setOpenPDF] = useState(false)


    const handleClose = ()=>{
        setOpenPDF(false)
    }

    return (
        <div className={styles.service}>
            <div className={styles.title}>
                <h1 title='WEST ADVISORY | Sản phẩm và dịch vụ BIM MEP'>Sản phẩm và Dịch vụ</h1>
                <p title='WEST ADVISORY | Các dịch vụ BIM MEP'>Một số sản phẩm nổi bật của chúng tôi</p>
                <p title='WEST ADVISORY | Khoá học Revit Mep'>Xây dựng và training addin Revit Mep, Giải pháp tự động hóa Revit Mep cho cá nhân hoặc doanh nghiệp làm Bim</p>
            </div>
           
            <div className={`${styles.items} `}>
                <div className={`${styles.listItem} `}>
                    <div className={`${styles.item} ${styles.bg1} `}>
                        <div className={styles.imageIC}>
                            <Image
                                alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                                src={TrainingLogo}
                                placeholder="blur"
                                width={0}
                                height={0}
                                sizes="100vw"
                                style={{ width: '100%', height: 'auto' }}
                            /> 
                        </div>
                        <div className={styles.titleItem}>
                            <p>
                                HqlTools
                            </p>
                        </div>
                        <div className={styles.content}>
                            <div>
                                {content1.length > 200 && isMore1 ? content1.slice(0,200) + "..." :content1}
                            </div>
                        </div>
                        <div className={`${styles.show}`} onClick={()=>setIsMore1(!isMore1)}>{isMore1 ? "Show more" : "Show Less"} <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
                <div className={styles.listItem}>
                    <div className={`${styles.item} ${styles.bg2}`}>
                        <div className={styles.imageIC}>
                            {/* <Image image={"https://bimdev.vn/wp-content/uploads/2020/02/revit-addin-steps-theme-1-e1581517115327.png"} contain='cover'/> */}
                            <Image
                                alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                                src={TrainingLogo}
                                placeholder="blur"
                                width={0}
                                height={0}
                                sizes="100vw"
                                style={{ width: '100%', height: 'auto' }}
                            /> 
                       
                        </div>
                        <div className={styles.titleItem}>
                            <p >  Xây Dựng ADDIN</p>
                        </div>
                        <div className={styles.content}>
                            <p>{content2.length > 200 && isMore2 ? content2.slice(0,200) + "..." :content2}</p>
                        </div>
                        <div className={`${styles.show}`} onClick={()=>setIsMore2(!isMore2)}>{isMore2 ? "Show more" : "Show Less"} <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
                <div className={styles.listItem}>
                    <div className={`${styles.item} ${styles.bg3}`}>
                        <div className={styles.imageIC}>
                            
                            <Image
                                alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                                src={Training1}
                                placeholder="blur"
                                width={0}
                                height={0}
                                sizes="100vw"
                                style={{ width: '100%', height: 'auto' }}
                            /> 
                        </div>
                        <div className={styles.titleItem}>
                            <p >Training REVIT API</p>
                        </div>
                        <div className={styles.content}>
                            <p>{content3.length > 200 && isMore3 ? content3.slice(0,200) + "..." :content3}</p>
                        </div>
                        <div className={`${styles.show}`} onClick={()=>setIsMore3(!isMore3)}>{isMore3 ? "Show more" : "Show Less"}  <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
                <div className={styles.listItem}>
                    <div className={`${styles.item} ${styles.bg4}`} onClick={()=>setOpenPDF(true)}>
                        <div className={styles.imageIC}>
                            <Image
                                alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                                src={Training2}
                                placeholder="blur"
                                width={0}
                                height={0}
                                sizes="100vw"
                                style={{ width: '100%', height: 'auto' }}
                            /> 
                        </div>
                        <div className={styles.titleItem}>
                            <p>Training REVIT MEP</p>
                        </div>
                        <div className={styles.content}>
                            <p>{content4.length > 200 && isMore4? content4.slice(0,200) + "..." :content4}</p>
                        </div>
                        <div className={`${styles.show}`} onClick={()=>setIsMore4(!isMore4)}>{isMore4 ? "Show more" : "Show Less"}  <i className="fa-sharp fa-solid fa-caret-down"></i></div>
                    </div>
                </div>
            </div>
            
            {
                openPDF ?  <div className={styles.viewPdf}>

                <MyPdfViewer handleClose={handleClose} />
            </div> : <></>
            }
           
        </div>
  )
}

export default Service