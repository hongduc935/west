"use client"

import { Swiper, SwiperSlide } from "swiper/react";
import Banner1 from '../../../../assets/images/revit-mep-plugin.png'
import "swiper/css";
import "swiper/css/navigation";
import styles from './styles/banner.module.scss'
// import required modules
import { Navigation } from "swiper";
import Image from 'next/image'

const Banner = () => {
  return (
    <div className={styles.banner}>
        {/* <Swiper navigation={true} modules={[Navigation]} className={`${styles.mySwiperBanner} `}>
        <SwiperSlide>
            <div className={styles.image}>
                <Image
                    alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                    src={'/revit-mep-plugin.png'}
                    // placeholder="blur"
                    width={0}
                    height={0}
                    sizes="100vw"

                    style={{ width: '100%', height: 'auto' }}
                  /> 
            </div>
        </SwiperSlide>
      </Swiper> */}
      <div className={styles.bannerBim}>
      <Image
                    alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                    src={'/revit-mep-plugin.png'}
                    // placeholder="blur"
                    width={100}
                    height={0}
                    sizes="100vw"

                    style={{ width: '100%', height: '600px' }}
                  /> 

      </div>
    </div>
  )
}

export default Banner