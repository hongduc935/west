import React from 'react'
import styles from './styles/blog.module.scss'

import Image from 'next/image'
import Link from 'next/link'

const Blogs = () => {
  return (
    <div className={styles.blogs}>
       
        <h2 className={styles.titleSection}>Cập nhật tin tức về Bim Mep </h2>
        <div className={styles.listCard}>
            <div className={styles.cardItem}>
                <div className={styles.previewImage}>
                    <Image   
                    alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                    src={'/vlu-workshop.webp'}
                    width={100}
                    height={0}
                    sizes="100vw"
                    style={{ width: '100%', height: '100%' }}/>
                </div>
                <h3 className={styles.title}>WORKSHOP Tại VLU </h3>
                <p className={styles.shortContent}> Tối ưu hóa công việc BIM MEP với ứng dụng tự động hóa từ HQL - Revit MEP Plugins </p>
                <Link href={'http://hqlbimmepsolutions.com'}><button className={styles.btnMore}>Đọc thêm </button></Link>
            </div>
            <div className={styles.cardItem}>
                <div className={styles.previewImage}>
                    <Image   
                    alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                    src={'/worksho-searefico.jpg'}
                    // placeholder="blur"
                    width={100}
                    height={0}
                    sizes="100vw"
                    style={{ width: '100%', height: '100%' }}/>
                </div>
                <h3 className={styles.title}>WORKSHOP Tại Searefico</h3>
                <p className={styles.shortContent}> Hành trình mang HqlTools  đến với kỹ sư BIM MEP tại công ty Searefico.
HqlTools, Tự Tin - Tăng Tốc - Tiết kiệm cùng kỹ sư vượt qua mọi dự án. </p>
                <Link href={'http://hqlbimmepsolutions.com'}><button className={styles.btnMore}>Đọc thêm </button></Link>
            </div>
            <div className={styles.cardItem}>
                <div className={styles.previewImage}>
                    <Image   
                    alt="WEST ADVISORY - HQL PLUGIN BIM MEP REVIT SOLUTION"
                    src={'/workshop-company.jpeg'}
                    // placeholder="blur"
                    width={100}
                    height={0}
                    sizes="100vw"

                    style={{ width: '100%', height: '100%' }}/>
                </div>
                <h3 className={styles.title}>Chuỗi hành trình WORKSHOP </h3>
                <p className={styles.shortContent}> HqlTools Tiếp tục trong chuỗi chương trình mang tự động hóa BIM MEP vào các công ty thiết kế. </p>
                <Link href={'http://hqlbimmepsolutions.com'}><button className={styles.btnMore}>Đọc thêm </button></Link>
            </div>
            
        </div>
    </div>
  )
}

export default Blogs