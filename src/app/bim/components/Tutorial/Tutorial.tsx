"use client"
import styles from './styles/tutorial.module.scss'
import "swiper/css"
import "swiper/css/free-mode"
import "swiper/css/pagination"
import ReactPlayer from "react-player"
import { Swiper, SwiperSlide } from "swiper/react"
import Preview from './component/Preview/Preview'
import SwiperCore,{  Pagination,Autoplay ,Navigation} from "swiper"
import { useState ,useRef} from 'react'
import ListTutorialDB from './constants'
SwiperCore.use([Autoplay])


const Tutorial = () => {
    const navigationPrevRef =useRef(null)
    const navigationNextRef =useRef(null)
    let [isHiddenPreview,setIsHiddenPreview] = useState<Boolean>(false)
    const handleOpenPreview = ()=>{
        setIsHiddenPreview(true)
    }
  return (
    <div className={styles.tutorial}>
        <div className={styles.title}>
            <h1 title={'WEST ADVISORY | Sản phầm BIM MEP'}>Sản Phẩm Nội Bật</h1>
        </div>
        <div className={styles.note}>
            <p title={"WEST ADVISORY | Tool Bim Mep"}>Một số công cụ hỗ trợ dựng hình hệ thống MEP </p>
        </div>
        <div className={styles.posts}>
                <div className={styles.previous} ref={navigationPrevRef}><i className="fa-solid fa-chevron-left"/></div>
                <div className={styles.next} ref={navigationNextRef}><i className="fa-solid fa-chevron-right"/></div>
                <>
                    <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        autoplay={{
                          delay: 2500,
                          disableOnInteraction: false,
                        }}
                        pagination={{
                          clickable: true,
                        }}
                        modules={[Autoplay, Pagination, Navigation]}
                        className="mySwiper"
                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                          }}
                        onSwiper={(swiper:any) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current
                            swiper.params.navigation.nextEl = navigationNextRef.current
                            swiper.navigation.destroy()
                            swiper.navigation.init()
                            swiper.navigation.update()
                        }}
                    >
                        {ListTutorialDB.flatMap((value:any,index:number)=>{
                            return <SwiperSlide key={`tutorial`+index}>
                                        <div className={styles.posted} onClick={handleOpenPreview} >   
                                            <div className={styles.image}>
                                                <ReactPlayer
                                                    width={"100%"}
                                                    height={220}
                                                    url={value.url}
                                                />
                                            </div>
                                            <div className={styles.mainPost}>
                                                <p className={styles.title}>{value.title.length > 30 ? value.title.slice(0,20)+'...'  :value.title }</p>
                                                <div></div>
                                                <p  className={styles.description}>{value.description.length > 60 ?  value.description.slice(0,60)+'...' : value.description}</p>    
                                            </div>
                                        </div>    
                                </SwiperSlide>
                        }) }
                    </Swiper>
            </>     
        </div>
        {isHiddenPreview ? <Preview handleClose={()=>setIsHiddenPreview(false)}/>:<></>}
    </div>
  )
}

export default Tutorial


