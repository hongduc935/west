import React from 'react'
import styles from './styles/preview.module.scss'

const Preview = (props:any) => {
  return (
    <div className={`${styles.preview} item-center`}>
        <div className={styles.close} onClick={props?.handleClose}>
            <div className={styles.closemain}>
                <i className="fa-solid fa-xmark"></i>
            </div>
        </div>
        <div className={styles.mainPreview}>
            <iframe className="mfp-iframe" src="//www.youtube.com/embed/7-tbc5NcQqM?autoplay=1" frameBorder={0} allowFullScreen={true} ></iframe>
        </div>
        Preview
    </div>
  )
}

export default Preview