import React from 'react'
import styles from './styles/mobile-service.module.scss'
import Image from '@/components/Image/Image'
import Head from 'next/head'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAndroid, faApple, faReact} from '@fortawesome/free-brands-svg-icons'
const MobileService = () => {
  return (
<>

    <Head>
            <title>Professional Web Services | WEST ADVISORY</title>
            <meta name="description" content="Explore top-notch web services with WEST ADVISORY, enhancing your online presence, driving traffic & converting visitors into loyal customers. Tailored solutions for your digital needs." />

            {/* Open Graph */}
            <meta property="og:title" content="Expert Web Services | WEST ADVISORY" />
            <meta property="og:description" content="Unveil a range of expert web services from WEST ADVISORY to boost your digital footprint, elevate traffic, and transform visitors into dedicated customers. Your digital journey, redefined with us." />
            <meta property="og:image" content="https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/377251486_132909326565504_3886310510995379284_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=eDBJEAd79s4AX-NRUb2&_nc_ht=scontent.fsgn5-9.fna&_nc_e2o=f&oh=00_AfAZLf6T1UOdhAiWQc_UN228tMoIEE6K70ljjvj5HOTwGw&oe=652DC00B" />
            <meta property="og:url" content="https://www.west-advisory.com/banner.webp" />

            {/* Twitter Card */}
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@your_twitter" />
            <meta name="twitter:title" content="Leading Web Services | WEST ADVISORY" />
            <meta name="twitter:description" content="Embark on a digital voyage with WEST ADVISORY. Avail bespoke web services, escalating your web presence, amplifying traffic, and converting visitors into faithful clients. Experience a digital transformation like never before." />
            <meta name="twitter:image" content="https://www.west-advisory.com/banner.webp" />


            <meta name="description" content="Ignite your online presence with WEST ADVISORY – Your trusted partner in web design, development, and digital marketing. Tailoring innovative and results-driven digital solutions that elevate your brand." />

            {/* Open Graph */}
            <meta property="og:title" content="Revolutionizing Your Online Identity | WEST ADVISORY" />
            <meta property="og:description" content="Step into a world where your brand's online existence echoes with clarity and technological prowess. WEST ADVISORY is your ally in constructing a digital persona that resonates with your audience." />
            <meta property="og:image" content="https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/377251486_132909326565504_3886310510995379284_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=eDBJEAd79s4AX-NRUb2&_nc_ht=scontent.fsgn5-9.fna&_nc_e2o=f&oh=00_AfAZLf6T1UOdhAiWQc_UN228tMoIEE6K70ljjvj5HOTwGw&oe=652DC00B" />
            <meta property="og:url" content="https://www.facebook.com/permalink.php?story_fbid=151025114753925&id=100095393118824" />
    </Head>
    <div className={`${styles.listService} item-center `}>
                <div className={styles.service} >

                    <div className={styles.titleService}>
                        <h1>Mobile App Development Services</h1>
                    </div>
                    <div className={`${styles.listIcons} item-btw`}>

                        <div className={styles.itemIcon}>
                            <FontAwesomeIcon icon={faApple} />
                            <p className={styles.noteApp}>Iphone App Development</p>
                        </div>
                        <div className={styles.itemIcon}>
                            <FontAwesomeIcon icon={faReact} />
                            <p className={styles.noteApp}>React Native Development</p>
                        </div>
                        <div className={styles.itemIcon}>
                            <FontAwesomeIcon icon={faAndroid} />
                            <p className={styles.noteApp}>Android App Development</p>
                        </div>

                    </div>
                   
                   

                    <div className={`${styles.image} item-center`} >
                        <div className={styles.img}>
                            <Image image='/app.png' contain='' />
                        </div>

                    </div>
                    <div className={styles.title}>
                        <div className={styles.s}>


                        </div>
                    </div>
                    
                </div>
            </div>
            </>
  )
}

export default MobileService