"use client"
import React from 'react'
import styles from './styles/what-is-west.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAnglesRight } from '@fortawesome/free-solid-svg-icons'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import Image from '@/components/Image/Image'
import { Pagination } from 'swiper'
import SwiperCore, { Autoplay } from 'swiper'

interface DataWhatIsWest{
    url:string 
    title:string
}
const WhatIsWestImageList:DataWhatIsWest[] = [
    {
        url:"https://i.pinimg.com/736x/25/2d/eb/252deb716473c370644ab60bbfb29dd7.jpg",
        title:"Professional Services Development"
    },
    {
        url:"https://images.assetsdelivery.com/compings_v2/evgeniyasheydt/evgeniyasheydt2301/evgeniyasheydt230100331.jpg",
        title:"Innovation and Creativity"
    },
    {
        url:"https://img.freepik.com/premium-vector/technology-computing-cartoon_24640-41970.jpg?w=2000",
        title:"New Technology Application"
    },
 
]

const WhatIsWest = () => {
  return (
    <section className={styles.whatWest}>
                    <div className={`${styles.layoutWhat} item-btw w-100`}>
                        <div className={`${styles.nameCompany} w-30`}>
                            <h3 className={styles.name}>WHAT IS WEST ? </h3>
                            <span className={styles.iconNext}>
                                <FontAwesomeIcon icon={faAnglesRight} fade />
                                <FontAwesomeIcon icon={faAnglesRight} fade />
                            </span>
                        </div>
                        <div className={`${styles.productMain} w-70`}>

                            <Swiper
                                slidesPerView={3}
                                spaceBetween={20}
                                pagination={{
                                    clickable: true,
                                }}
                                modules={[Pagination]}
                                className="aboutUsWhatSwiper"
                                autoplay={true}
                                loop={true}
                            >
                                {WhatIsWestImageList.flatMap((data:DataWhatIsWest,index:number)=>{
                                        return    <SwiperSlide key={`whatiswest${index}`}>
                                                        <div className={styles.itemProduction}>
                                                            <div className={`${styles.number} `}>
                                                                <span className={styles.element}>0{index+1}</span>
                                                            </div>
                                                            <div className={styles.image}>
                                                                <Image image={data.url} />
                                                            </div>
                                                            <div className={styles.des}>
                                                                <p className={`txt-center`}>{data.title}</p>
                                                            </div>
                        
                                                        </div>
                                                    </SwiperSlide>
                                })}
                            </Swiper>
                        </div>
                    </div>
                    <div className={styles.bgName}>
                        <h2>WEST ADVISORY</h2>
                    </div>

                </section>
  )
}

export default WhatIsWest