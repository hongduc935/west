// "use client"
import React from 'react'
import styles from './styles/about-us.module.scss'
import Image from '@/components/Image/Image'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import { Pagination } from 'swiper'
import SwiperCore, { Autoplay } from 'swiper'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAnglesRight, faQuoteLeft, faQuoteRight } from '@fortawesome/free-solid-svg-icons'
import InfiniteLooper from '../../components/InfiniteLooper/InfiniteLooper'

// import Head from 'next/head';
import { Metadata } from 'next'
import WhatIsWest from './components/WhatIsWest/WhatIsWest'

// import required modules
SwiperCore.use([Autoplay]);

const PartnerList = [
    "./dataholics-logo.jpg",
    "./shsoft-vina.png",
    "./optimus-logo.png",
    "./mibo.png",
    "./zoho-logo.png",
    "./tsp.webp"
]
const TeamWorkImageList = [
    "./teamwork-about-us.jpeg",
    "./best-practice-about-us.jpeg",
    "./positive-about-us.jpeg",
    "./best-practice-about-us.jpeg"
]

interface DataWhatIsWest{
    url:string 
    title:string
}
const WhatIsWestImageList:DataWhatIsWest[] = [
    {
        url:"https://i.pinimg.com/736x/25/2d/eb/252deb716473c370644ab60bbfb29dd7.jpg",
        title:"Professional Services Development"
    },
    {
        url:"https://images.assetsdelivery.com/compings_v2/evgeniyasheydt/evgeniyasheydt2301/evgeniyasheydt230100331.jpg",
        title:"Innovation and Creativity"
    },
    {
        url:"https://img.freepik.com/premium-vector/technology-computing-cartoon_24640-41970.jpg?w=2000",
        title:"New Technology Application"
    },
 
]



// Metadata for the About Us page
export const metadata: Metadata = {
    title: 'WEST ADVISORY - Chuyên Gia Gia Công Phần Mềm | CRM, Web & Mobile Apps, Bim Mep Solutions',
    description: 'WEST ADVISORY - Chuyên gia phần mềm CRM, Web, Mobile Apps, Bim Mep. Giải pháp tùy chỉnh chất lượng cao.',
};
  

const AboutUS = () => {
    return (
        <>
       
            <div className={styles.aboutUs}>
                
                <section className={`${styles.banner} item-center`}>
                    <div className={styles.mainContent}>
                        <h1 className={styles.title}>WEST ADVISORY </h1>
                        <p className={styles.shortDes}>We established the company to provide CRM technology solutions. Our operations are based on the criteria that product .</p>
                    </div>
                </section>
                {/* <section className={styles.whatWest}>
                    <div className={`${styles.layoutWhat} item-btw w-100`}>
                        <div className={`${styles.nameCompany} w-30`}>
                            <h3 className={styles.name}>WHAT IS WEST ? </h3>
                            <span className={styles.iconNext}>
                                <FontAwesomeIcon icon={faAnglesRight} fade />
                                <FontAwesomeIcon icon={faAnglesRight} fade />
                            </span>
                        </div>
                        <div className={`${styles.productMain} w-70`}>

                            <Swiper
                                slidesPerView={3}
                                spaceBetween={20}
                                pagination={{
                                    clickable: true,
                                }}
                                modules={[Pagination]}
                                className="aboutUsWhatSwiper"
                                autoplay={true}
                                loop={true}
                            >
                                {WhatIsWestImageList.flatMap((data:DataWhatIsWest,index:number)=>{
                                        return    <SwiperSlide key={`whatiswest${index}`}>
                                                        <div className={styles.itemProduction}>
                                                            <div className={`${styles.number} `}>
                                                                <span className={styles.element}>0{index+1}</span>
                                                            </div>
                                                            <div className={styles.image}>
                                                                <Image image={data.url} />
                                                            </div>
                                                            <div className={styles.des}>
                                                                <p className={`txt-center`}>{data.title}</p>
                                                            </div>
                        
                                                        </div>
                                                    </SwiperSlide>
                                })}
                            </Swiper>
                        </div>
                    </div>
                    <div className={styles.bgName}>
                        <h2>WEST ADVISORY</h2>
                    </div>

                </section>
             */}
                <WhatIsWest/>
                <section className={`${styles.ourTeam} w-100 item-btw`}>
                    <div className={`${styles.contentTeam} w-50`}>
                        <h2 className={styles.titleSection}>WE ARE TEAM  </h2>
                        <p className={styles.expansion}>
                            At WEST ADVISORY, our diverse team of skilled professionals with varied cultural backgrounds and perspectives is our strength. This diversity fuels creativity and broadens our approach to challenges, driving innovation and excellence.
                        </p>

                    </div>
                    <div className={`${styles.imageTeam} w-50`}>
                        <InfiniteLooper speed={10} direction="right">
                            {TeamWorkImageList.flatMap((url:string,index:number)=>{
                                return   <div className="contentBlock contentBlock--one" key={`teamwork${index}`}>
                                            <div className={styles.imageLoop}>
                                                <Image image={url} />
                                            </div>
                                        </div>
                            })}
                        </InfiniteLooper>
                    </div>
                </section>
                <section className={styles.sectionQuote}>
                        <h2 className={`${styles.title}`}>What Our Partners Say About Us </h2>
                        <div className={`${styles.quotes} item-btw w-100`}>
                            <div className={`${styles.quote} `}>
                                <div className={`${styles.imageProfile}`}>
                                    <div className={`${styles.image} ${styles.image0}`}>
                                        
                                    </div>
                                </div>
                                <div className={`${styles.info}`}>
                                    <p className={styles.name}>Jerry Edmonds</p>
                                    <p className={styles.pos}>Chief Executive Officer (CEO) & Founder at TSP Contracting</p>
                                    <p className={styles.content}> <FontAwesomeIcon icon={faQuoteLeft} /> We greatly appreciate the flexibility and collaborative spirit of the company in meeting the specific requirements of the Zoho CRM project. They consistently listen to and understand our needs, thereby providing the most suitable and effective solutions. <FontAwesomeIcon icon={faQuoteRight} /></p>
                                </div>
                            </div>

                            <div className={`${styles.quote} `}>
                                <div className={`${styles.imageProfile}`}>
                                    <div className={`${styles.image} ${styles.image1}`}>
                                        
                                    </div>
                                </div>
                                <div className={`${styles.info}`}>
                                    <p className={styles.name}>황명욱</p>
                                    <p className={styles.pos}>Chief Executive Officer (CEO) & Founder at SHSOFT VINA</p>
                                    <p className={styles.content}><FontAwesomeIcon icon={faQuoteLeft} /> We are very satisfied with the results achieved; the Zoho CRM system is not only stable but also user-friendly and well-suited to our business needs. This has helped us enhance work efficiency and achieve important business objectives. <FontAwesomeIcon icon={faQuoteRight} /></p>
                                </div>
                            </div>
                        </div>
                        
                </section>
                <section className={styles.sectionPartner}>
                    <p className={styles.note}>Partners</p>
                    <div className={styles.listPartner}>
                        <InfiniteLooper speed={10} direction="right">
                                {PartnerList.flatMap((url:string,index:number)=>{
                                    return  <div className="contentBlock contentBlock--one" key={`partner${index}`}>
                                                <div className={styles.imageLoop}>
                                                    <Image image={url} contain='contain' />
                                                </div>
                                            </div>
                                })}
                        </InfiniteLooper>
                    </div>

                </section>
            </div>
        </>
        
    )
}

export default AboutUS