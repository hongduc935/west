"use client"
import styles from './styles/send-message.module.scss'
import { useForm, SubmitHandler } from "react-hook-form"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPhone, faLocation, faEnvelope } from '@fortawesome/free-solid-svg-icons'
type Inputs = {
    name: string,
    email: string,
    content: string
};
const SendMessage = () => {
    const { register, handleSubmit, formState: { errors } } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {

        } catch (error) {
        }
    };
    return (
        <div className={`${styles.sendMessage} `}>
            <div className={styles.nav}>
                <h2>SEND MESSAGE</h2>
            </div>
            <div className={`${styles.mainSend} item-btw w-100`}>
                <div className={`${styles.form} w-50`}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className={styles.formField}>
                            <input  {...register("name")} placeholder='Name' />
                        </div>
                        <div className={styles.formField}>
                            <input {...register("email", { required: true })} placeholder='Email' />
                        </div>
                        <div className={styles.formField}>
                            <textarea {...register("content", { required: true })} placeholder='Message' />
                        </div>
                        {/* <div className={styles.formField1}>
                            <input placeholder='Message' type='checkbox' /> &nbsp; I agree with the policy
                        </div> */}
                        <div className={styles.buttonForm}>
                            <button type='submit'>Send</button>
                        </div>
                    </form>
                </div>
                <div className={`${styles.info} w-40`}>
                    <p> <FontAwesomeIcon icon={faPhone} />  &nbsp;0354298203</p>
                    <p> <FontAwesomeIcon icon={faEnvelope} />  &nbsp;hongduc935@gmail.com</p>
                    <p> <FontAwesomeIcon icon={faLocation} /> &nbsp; 20/14 Duong 13 Linh Chieu Thu Đuc</p>
                   
                </div>
            </div>
        </div>
    )
}

export default SendMessage