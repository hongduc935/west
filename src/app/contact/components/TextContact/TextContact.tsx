import styles from './styles/text-contact.module.scss'

const TextContact = () => {
  return (
    <div className={`${styles.textContact} container`}>
      <p className={styles.title}>CONTACT</p>
      <ul className={styles.menu}>
        <li>Thank you for your interest in <b>WEST ADVISORY</b> 's Products and Services. </li>
        <li>We are always ready to consult and address any questions from our valued customers.</li>
        <li>Please leave your information, and we will contact you as soon as possible.</li>
        <li>Whatsapp:&nbsp;<span className={styles.phone}>+84 559 242 538</span> </li>
        <li>Email:&nbsp;<span className={styles.thank}></span>contact.westadvisory@gmail.com</li>
        <li className={styles.thank}>You're sincerely appreciated!</li>
      </ul>
    </div>
  )
}
export default TextContact