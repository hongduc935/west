import styles from './styles/map.module.scss'
const Map = () => {
  return (
    <div className={styles.map}>
      <iframe id="contact_iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3920.195209904025!2d106.60609291494148!3d10.71942166317415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ded0b186f3b%3A0x8f99bca98f0d9c00!2zNzcgVsO1IFbEg24gS2nhu4d0LCBBbiBM4bqhYywgQsOsbmggVMOibiwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oIDcxOTA2LCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1684215747001!5m2!1sen!2s" width="600" height="600" frameBorder={0} scrolling="no" marginHeight={0} marginWidth={0} allowFullScreen={true}></iframe>
        
    </div>
  )
}

export default Map