import React from 'react'
import styles from './styles/contact.module.scss'
import TextContact from './components/TextContact/TextContact'
const Contact = () => {
  return (
    <div className={styles.contact}>
      <TextContact />
    </div>
  )
}

export default Contact