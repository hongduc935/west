import React from 'react'
import Image from '@/components/Image/Image'
import styles from './styles/web-service.module.scss'
import Head from 'next/head'
import { Metadata } from 'next';

export const metadata: Metadata = {
    title: "Professional Website Design Services | West Advisory",
    description: "Make a strong impression with professional website design services from West Advisory. We offer custom website design solutions, SEO optimization, and excellent user experience to help your business stand out online. Contact us today for a free quote and consultation!",
    keywords: "website design, web design services, professional web design, SEO optimization, custom web design, West Advisory",
    // openGraph: {
    //     title: "Professional Website Design Services | West Advisory",
    //     description: "Make a strong impression with professional website design services from West Advisory. We provide custom website design solutions and SEO optimization. Contact us today!",
    //     images: [
    //         {
    //             url: "https://www.west-advisory.com/your-image.png",
    //             alt: "Professional website design services from West Advisory"
    //         }
    //     ],
    //     url: "https://www.west-advisory.com/website-design-services"
    // },
    // twitter: {
    //     card: "summary_large_image",
    //     site: "@west_advisory",
    //     title: "Professional Website Design Services | West Advisory",
    //     description: "West Advisory's website design services help you create a stunning and effective website. SEO optimization and excellent user experience.",
    //     images: [
    //         "https://www.west-advisory.com/your-image.png"
    //     ]
    // }
};


const Webservice = () => {
  return (
    <>
   

   <main className={styles.container}>
        <section className={styles.hero}>
          <h1>Professional Website Design Services</h1>
          <p>
            At West Advisory, we specialize in creating exceptional websites that make a lasting impression. Our professional website design services offer custom solutions tailored to your business needs, ensuring that your website stands out from the competition and provides an outstanding user experience.
          </p>
          <a href="#contact" className={styles.cta}>Get a Free Quote</a>
        </section>

        <section className={styles.services}>
          <h2>Why Choose West Advisory?</h2>
          <ul>
            <li><strong>Custom Designs:</strong> We provide tailor-made solutions that align with your brand and business objectives, creating a unique online presence.</li>
            <li><strong>SEO Optimization:</strong> Our designs are optimized for search engines to help your website rank higher and attract more organic traffic.</li>
            <li><strong>Excellent User Experience:</strong> We focus on creating user-friendly websites that engage visitors and drive conversions.</li>
            <li><strong>Ongoing Support:</strong> Our team offers continuous support and maintenance to ensure your website remains up-to-date and performs optimally.</li>
            <li><strong>Responsive Design:</strong> We ensure that your website looks and functions perfectly on all devices, from desktops to smartphones.</li>
            <li><strong>Fast Loading Speed:</strong> Our websites are optimized for fast loading times, enhancing user experience and reducing bounce rates.</li>
          </ul>
        </section>

        <section className={styles.process}>
          <h2>Our Design Process</h2>
          <ol>
            <li><strong>Consultation:</strong> We start by understanding your requirements, goals, and vision for your website. This helps us craft a design plan that suits your needs.</li>
            <li><strong>Planning:</strong> We create a detailed design plan, including wireframes and a project timeline, for your review and approval. This ensures that we are aligned with your expectations.</li>
            <li><strong>Design:</strong> Our creative team designs a custom website layout and visual elements that align with your brand identity. We focus on aesthetics, usability, and functionality.</li>
            <li><strong>Development:</strong> We build and develop the website using the latest technologies and best practices to ensure functionality, responsiveness, and security.</li>
            <li><strong>Testing and Launch:</strong> We thoroughly test your website across different devices and browsers to ensure everything works perfectly. Once tested, we launch your website and monitor its performance.</li>
            <li><strong>Post-Launch Support:</strong> We provide ongoing support and maintenance to address any issues, implement updates, and make necessary enhancements to keep your website performing at its best.</li>
          </ol>
        </section>

        <section className={styles.portfolio}>
          <h2>Our Recent Projects</h2>
          <div className={styles.projectsGrid}>
            <div className={styles.project}>
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmnuhHGD5siv6fB38Yvqij2e6QHMQBemQzdA&s" alt="Project 1" />
              <div className={styles.projectInfo}>
                <h3>Project 1</h3>
                <p>We designed a modern, responsive website for a leading e-commerce company, boosting their online sales by 40% within the first three months.</p>
              </div>
            </div>
            <div className={styles.project}>
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmnuhHGD5siv6fB38Yvqij2e6QHMQBemQzdA&s" alt="Project 2" />
              <div className={styles.projectInfo}>
                <h3>Project 2</h3>
                <p>Our team created a visually stunning portfolio site for a creative agency, showcasing their work and attracting new clients.</p>
              </div>
            </div>
            <div className={styles.project}>
              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmnuhHGD5siv6fB38Yvqij2e6QHMQBemQzdA&s" alt="Project 3" />
              <div className={styles.projectInfo}>
                <h3>Project 3</h3>
                <p>We developed a user-friendly website for a local restaurant, complete with online reservations and menu management, leading to a significant increase in reservations.</p>
              </div>
            </div>
          </div>
        </section>

        <section className={styles.testimonials}>
          <h2>What Our Clients Say</h2>
          <blockquote>
            <p>"West Advisory transformed our online presence with their exceptional website design services. Their attention to detail and dedication to our project was impressive. We highly recommend them!"</p>
            <footer>- Jane Doe, CEO of Example Corp</footer>
          </blockquote>
          <blockquote>
            <p>"The team at West Advisory delivered a fantastic website that exceeded our expectations. Their expertise in web design and SEO helped us attract more clients and grow our business."</p>
            <footer>- John Smith, Founder of Smith Enterprises</footer>
          </blockquote>
        </section>

        <section id="contact" className={styles.contact}>
            <h2>Contact Us</h2>
            <p>
                Ready to elevate your online presence with a professionally designed website? Contact West Advisory today for a free quote and consultation. Our team is here to help you create a stunning and effective website that meets your business needs.
            </p>
            <div className={styles.contactContent}>
                <div className={styles.contactItem}>
                <h3>Phone</h3>
                <p><strong>0354298203</strong></p>
                </div>
                <div className={styles.contactItem}>
                <h3>Email</h3>
                <p><strong>hongduc935@gmail.com</strong></p>
                </div>
                <div className={styles.contactItem}>
                <h3>Address</h3>
                <p><strong>82 Hoàng Trọng Mậu , Him Lam , Tân Hưng, Quận 7, TPHCM</strong></p>
                </div>
            </div>
        </section>


      </main>
    </>
   
  )
}

export default Webservice