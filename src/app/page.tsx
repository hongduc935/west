import styles from './page.module.css'
import HomePage from './home/page'
import { Metadata } from 'next';
export const metadata: Metadata = {
  title: 'WEST ADVISORY - Premier Zoho CRM Implementation Partner in Vietnam',
  description: 'WEST ADVISORY is the leading provider and consultant for CRM projects, specializing in Zoho CRM implementation in Vietnam. As a certified Zoho partner, we deliver top-notch CRM solutions to drive business success.',
  keywords: 'WEST ADVISORY, Zoho CRM, CRM implementation, CRM consulting, Vietnam, Zoho partner, business solutions, customer relationship management',
  
};
export default function Home() {
  return (
    <main className={styles.main}>
      <HomePage/>
    </main>
  )
}
