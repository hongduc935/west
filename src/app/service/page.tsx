import React from 'react'
import styles from './styles/service.module.scss'
import HeaderService from './components/HeaderService/HeaderService'
import Head from 'next/head'
import { Metadata } from 'next'



export const metadata: Metadata = {
  title: "Zoho CRM Implementation Services by WEST ADVISORY: Your Path to Success",
  description: "Transform your business with Zoho CRM Implementation services by WEST ADVISORY. Our seasoned experts optimize processes, elevate customer interactions, and drive substantial business growth. Unlock your potential today with our bespoke Zoho CRM Implementation solutions and explore flexible pricing plans tailored to your needs.",
  keywords: "Zoho CRM, implementation, CRM solutions, pricing plans, WEST ADVISORY",
  // openGraph: {
  //     title: "Maximize Customer Relationships with Zoho CRM | WEST ADVISORY",
  //     description: "Elevate your business with Zoho CRM solutions from WEST ADVISORY. Our experts optimize processes, improve customer interactions, and drive substantial business growth. Explore our Zoho implementation pricing plans.",
  //     images: [
  //         {
  //             url: "https://www.west-advisory.com/CRM.png",
  //             alt: "Zoho CRM Implementation"
  //         }
  //     ],
  //     url: "https://www.west-advisory.com/CRMService"
  // },
  // twitter: {
  //     card: "summary_large_image",
  //     site: "@your_twitter",
  //     title: "Maximize Customer Relationships with Zoho CRM | WEST ADVISORY",
  //     description: "Elevate your business with Zoho CRM solutions from WEST ADVISORY. Our experts optimize processes, improve customer interactions, and drive substantial business growth.",
  //     images: [
  //         "https://www.west-advisory.com/CRM.png"
  //     ]
  // }
};
const Service = () => {

  
  return (
    <div className={styles.service}>
        <HeaderService/>
    </div>
  )
}

export default Service