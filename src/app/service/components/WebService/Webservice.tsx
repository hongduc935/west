import React from 'react'
import Image from '@/components/Image/Image'
import styles from './styles/web-service.module.scss'
import Head from 'next/head'
const Webservice = () => {
  return (
    <>
    <Head>
            <title>Professional Web Services | WEST ADVISORY</title>
            <meta name="description" content="Explore top-notch web services with WEST ADVISORY, enhancing your online presence, driving traffic & converting visitors into loyal customers. Tailored solutions for your digital needs." />

            {/* Open Graph */}
            <meta property="og:title" content="Expert Web Services | WEST ADVISORY" />
            <meta property="og:description" content="Unveil a range of expert web services from WEST ADVISORY to boost your digital footprint, elevate traffic, and transform visitors into dedicated customers. Your digital journey, redefined with us." />
            <meta property="og:image" content="https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/377251486_132909326565504_3886310510995379284_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=eDBJEAd79s4AX-NRUb2&_nc_ht=scontent.fsgn5-9.fna&_nc_e2o=f&oh=00_AfAZLf6T1UOdhAiWQc_UN228tMoIEE6K70ljjvj5HOTwGw&oe=652DC00B" />
            <meta property="og:url" content="https://www.west-advisory.com/banner.webp" />

            {/* Twitter Card */}
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@your_twitter" />
            <meta name="twitter:title" content="Leading Web Services | WEST ADVISORY" />
            <meta name="twitter:description" content="Embark on a digital voyage with WEST ADVISORY. Avail bespoke web services, escalating your web presence, amplifying traffic, and converting visitors into faithful clients. Experience a digital transformation like never before." />
            <meta name="twitter:image" content="https://www.west-advisory.com/banner.webp" />


            <meta name="description" content="Ignite your online presence with WEST ADVISORY – Your trusted partner in web design, development, and digital marketing. Tailoring innovative and results-driven digital solutions that elevate your brand." />

            {/* Open Graph */}
            <meta property="og:title" content="Revolutionizing Your Online Identity | WEST ADVISORY" />
            <meta property="og:description" content="Step into a world where your brand's online existence echoes with clarity and technological prowess. WEST ADVISORY is your ally in constructing a digital persona that resonates with your audience." />
            <meta property="og:image" content="https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/377251486_132909326565504_3886310510995379284_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=eDBJEAd79s4AX-NRUb2&_nc_ht=scontent.fsgn5-9.fna&_nc_e2o=f&oh=00_AfAZLf6T1UOdhAiWQc_UN228tMoIEE6K70ljjvj5HOTwGw&oe=652DC00B" />
            <meta property="og:url" content="https://www.facebook.com/permalink.php?story_fbid=151025114753925&id=100095393118824" />
    </Head>

    <div className={styles.webService}>
        

    </div>

      {/* <div className={`${styles.listService} item-btw container`}>
    <div className={styles.service} >
        <div className={`${styles.image} item-center`} >
            <div className={styles.img}>
                <Image image='https://uploads-ssl.webflow.com/615af81f65d1ab72d2969269/62efdf9840dca733692cdd48_web%20dev%20basics.jpg' contain='' />
            </div>
        </div>
        <div className={styles.title}>
            <p>WEB APPLICATION</p>
            <p className={styles.des}>
                <b>Navigate the Digital World with Unparalleled Web Solutions: </b>
            </p>
            <p className={styles.des}>
                <b>1. Bespoke Web Design:</b> <br /> Dive into a realm where each pixel speaks your brand language. Our bespoke web design tailors an online presence that is not just aesthetically appealing but also user-centric and conversion-optimized.
            </p>
            <p className={styles.des}>
                <b>2. Dynamic & Responsive Development:</b> <br /> Experience flawless functionality across all devices. Our web development ensures your digital platform is versatile, responsive, and impeccably adaptive to varied screen sizes and orientations.
            </p>
            <p className={styles.des}>
                <b>3. E-Commerce Excellence:</b> <br /> Transition your brick-and-mortar store to an e-commerce powerhouse. With secure payment gateways, intuitive UI/UX, and seamless checkout processes, we create online shopping experiences that customers adore.
            </p>
            <p className={styles.des}>
                <b>4. Data-Driven Decisions:</b> <br />  Leverage our analytical approaches to utilize your data smartly. Gain insights, understand your audience, and make strategic decisions that propel your business forward in the digital sphere.
            </p>
            <p className={styles.des}>
                <b>5. SEO Optimization: </b> <br /> Rise above the digital noise with our SEO strategies. Optimize your website to rank higher on search engines, drive organic traffic, and create brand visibility that resonates with your target audience.
            </p>
            <p className={styles.des}>
                <b>6. Content Management Simplicity: </b> <br />Manage, modify, and magnify your content with ease. With robust CMS solutions, ensure your website is always fresh, relevant, and engaging for your visitors.
            </p>
            <p className={styles.des}>
                <b>7. Security Assurance:</b> <br />Navigate the web with confidence. Our security protocols safeguard your digital presence against potential threats, ensuring data integrity and building customer trust.
            </p>
            <p className={styles.des}>
                <b>8. Scalability and Flexibility: </b> <br />Grow unbounded with our scalable web solutions. Ensure your website evolves in tandem with your business, always ready to meet the shifting demands of the digital landscape.
            </p>
            <p className={styles.des}>
                <b>9. 24/7 Support and Maintenance: </b> <br />Explore uncharted territories with our constant backing. With 24/7 support and meticulous maintenance, we ensure your digital presence is always in its prime.
            </p>
            <p className={styles.des}>
                In summary, CRM systems are invaluable tools for businesses seeking to build stronger customer relationships, streamline operations, and gain a competitive advantage in today's highly competitive market. They enable companies to better understand, engage, and serve their customers, ultimately leading to improved profitability and growth.
            </p>
        </div>
        
    </div>
</div> */}
    </>
   
  )
}

export default Webservice