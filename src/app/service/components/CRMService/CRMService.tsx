import React from 'react'
import styles from './styles/crm-service.module.scss'
import ScrollAppearEffect from '@/components/Effects/AppearEffect'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight, faHandPointRight } from '@fortawesome/free-solid-svg-icons'
import ZohoSheet from '../../../../assets/images/zoho-sheet.png'
import Image from 'next/image'
import Sleekflow from '../../../../assets/images/sleekflow.webp'
import ZohoSocial from '../../../../assets/images/zoho-social.png'
import ZohoSign from '../../../../assets/images/zoho-sign.png'
import ZohoWorkdrive from '../../../../assets/images/zoho-workdrive.jpeg'
import ZohoSalesiq from '../../../../assets/images/zoho-salesiq.png'
import ZohoBook from '../../../../assets/images/zoho-book.png'
import WhatsApp from '../../../../assets/images/whats-app.png'
import ZohoCreator from '../../../../assets/images/zoho-creator.png'
import ZohoOne from '../../../../assets/images/zoho-one.png'
import Xero from '../../../../assets/images/xero.png'
const CRMService = () => {
    return (
        <div className={`${styles.service}  container`}>

            <h1 className={styles.titleService}>ZOHO CRM IMPLEMENTATION</h1>
            <div className={`${styles.mainContent} item-btw`}>
                <div className={styles.leftContent}>
                    <ScrollAppearEffect time={1} effectX={-30}>
                        <div className={styles.image}>

                        </div>
                    </ScrollAppearEffect>

                </div>
                <div className={styles.rightContent}>
                    <h2 className={styles.titleBenefit}>Key Benefits of Zoho CRM</h2>
                    <ScrollAppearEffect time={1} effectX={-30}>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} /> Sales Automation: Automated sales processes, saving time and effort.</p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} /> Analysis and Reporting: Provides detailed data on shopping trends and business performance.</p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} /> Multi-Application Integration: Connects with email, calendar, social networks, and business applications.</p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} /> Flexible Customization: Interface and workflows customized according to business needs. </p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} /> User-Friendly Interface: Easy to use, minimizing training time.</p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} />Contact Management: Classify and track customer information in detail.</p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} />Marketing Campaign Management: Effectively plan, analyze, and track interactions.</p>
                        <p className={styles.lineBenefit}><FontAwesomeIcon icon={faHandPointRight} /> ptimal Customer Service: Online support, request management, and quick feedback.</p>
                        <div className='item-start'>
                            <button className={styles.moreService}>
                                Find out more <FontAwesomeIcon icon={faArrowRight} />
                            </button>
                        </div>
                    </ScrollAppearEffect>
                </div>

            </div>
            <div className={styles.integration}>
                <h1 className={styles.titleService}>CRM Integration</h1>
                <div className={styles.zohoProduct}>
                    <h4 className={styles.titleProduct}>ZOHO PRODUCT INTEGRATION</h4>
                    <div className={styles.layout}>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoSheet}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO SHEET</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoSocial}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO SOCIAL</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoWorkdrive}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO WORKDRIVE</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoSalesiq}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO SALE IQ</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>

                    </div>
                    <div className={styles.layout}>

                        <div className={styles.itemProduct}>

                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoSign}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO SIGN</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>


                        <div className={styles.itemProduct}>

                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoBook}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO BOOK</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>


                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoCreator}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO CREATOR</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>


                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Google Wokspace integrantion Zoho CRM"
                                    src={ZohoOne}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>ZOHO ONE</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>

                        </div>


                    </div>
                </div>
                <div className={styles.zohoProduct}>
                    <h4 className={styles.titleProduct}>THIRD PARTY INTEGRATION</h4>
                    <div className={styles.layout}>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - WhatsApp integrantion Zoho CRM"
                                    src={WhatsApp}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>WhatsApp</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Xero integrantion Zoho CRM"
                                    src={Xero}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>Xero </p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>
                        <div className={styles.itemProduct}>
                            <div className={styles.image}>
                                <Image
                                    alt="WEST ADVISORY - Sleekflow integrantion Zoho CRM"
                                    src={Sleekflow}
                                    placeholder="blur"
                                    width={0}
                                    height={0}
                                    sizes="100vw"
                                    style={{ width: '100%', height: '100%', objectFit: "contain" }}
                                />
                            </div>
                            <div className={`${styles.groupBtn} item-btw`}>
                                <p className={styles.nameProduct}>SLEEKFLOW</p><button className={styles.btnMoreProduct}>More Detail</button>
                            </div>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    )
}

export default CRMService