import React from 'react'
import styles from './styles/mobile-service.module.scss'
import Image from '@/components/Image/Image'
import Head from 'next/head'
const MobileService = () => {
  return (
<>

    <Head>
            <title>Professional Web Services | WEST ADVISORY</title>
            <meta name="description" content="Explore top-notch web services with WEST ADVISORY, enhancing your online presence, driving traffic & converting visitors into loyal customers. Tailored solutions for your digital needs." />

            {/* Open Graph */}
            <meta property="og:title" content="Expert Web Services | WEST ADVISORY" />
            <meta property="og:description" content="Unveil a range of expert web services from WEST ADVISORY to boost your digital footprint, elevate traffic, and transform visitors into dedicated customers. Your digital journey, redefined with us." />
            <meta property="og:image" content="https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/377251486_132909326565504_3886310510995379284_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=eDBJEAd79s4AX-NRUb2&_nc_ht=scontent.fsgn5-9.fna&_nc_e2o=f&oh=00_AfAZLf6T1UOdhAiWQc_UN228tMoIEE6K70ljjvj5HOTwGw&oe=652DC00B" />
            <meta property="og:url" content="https://www.west-advisory.com/banner.webp" />

            {/* Twitter Card */}
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@your_twitter" />
            <meta name="twitter:title" content="Leading Web Services | WEST ADVISORY" />
            <meta name="twitter:description" content="Embark on a digital voyage with WEST ADVISORY. Avail bespoke web services, escalating your web presence, amplifying traffic, and converting visitors into faithful clients. Experience a digital transformation like never before." />
            <meta name="twitter:image" content="https://www.west-advisory.com/banner.webp" />


            <meta name="description" content="Ignite your online presence with WEST ADVISORY – Your trusted partner in web design, development, and digital marketing. Tailoring innovative and results-driven digital solutions that elevate your brand." />

            {/* Open Graph */}
            <meta property="og:title" content="Revolutionizing Your Online Identity | WEST ADVISORY" />
            <meta property="og:description" content="Step into a world where your brand's online existence echoes with clarity and technological prowess. WEST ADVISORY is your ally in constructing a digital persona that resonates with your audience." />
            <meta property="og:image" content="https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/377251486_132909326565504_3886310510995379284_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=eDBJEAd79s4AX-NRUb2&_nc_ht=scontent.fsgn5-9.fna&_nc_e2o=f&oh=00_AfAZLf6T1UOdhAiWQc_UN228tMoIEE6K70ljjvj5HOTwGw&oe=652DC00B" />
            <meta property="og:url" content="https://www.facebook.com/permalink.php?story_fbid=151025114753925&id=100095393118824" />
    </Head>
    <div className={`${styles.listService} item-btw container`}>
                <div className={styles.service} >
                    <div className={`${styles.image} item-center`} >
                        <div className={styles.img}>
                            <Image image='/app.png' contain='' />
                        </div>
                    </div>
                    <div className={styles.title}>
                        <p>MOBILE APPLICATION</p>
                        <p className={styles.des}>
                            <b>Harness the Power of Mobility with Our Outstanding App Solutions: </b>
                        </p>
                        <p className={styles.des}>
                            <b>1. User-Centered Design:</b> <br /> Engage, enchant, and elevate user experiences with designs that are not only visually stunning but also intuitively functional, ensuring every tap and swipe is effortlessly seamless.
                        </p>
                        <p className={styles.des}>
                            <b>2. Cross-Platform Development:</b> <br /> Break barriers and embrace a wider audience with our cross-platform applications. Experience uniform functionality and design across iOS, Android, and other mobile platforms.
                        </p>
                        <p className={styles.des}>
                            <b>3. Immersive AR/VR Experiences:</b> <br /> Step into the future with our AR/VR app solutions. Create immersive experiences that transcend the ordinary, bridging the virtual and real worlds with innovative technologies.
                        </p>
                        <p className={styles.des}>
                            <b>4. IoT Integration:</b> <br />  Interlink your digital ecosystem with IoT-enabled apps. Experience a world where your application doesn’t just reside in phones but extends to wearables, smart devices, and beyond.
                        </p>
                        <p className={styles.des}>
                            <b>5. AI and Machine Learning: </b> <br /> Dive into intelligent app solutions powered by AI and ML. From personalized user experiences to smart automation, push the boundaries of what your app can achieve.
                        </p>
                        <p className={styles.des}>
                            <b>6. Data Security: </b> <br />Protect and preserve with stringent data security protocols. Our applications are built with robust security frameworks, ensuring your data is shielded and trust is built.
                        </p>
                        <p className={styles.des}>
                            <b>7. Agile Methodology: </b> <br />Experience the power of agility with our dynamic development approach. Ensure your app adapts, evolves, and scales in line with market trends and user expectations.
                        </p>
                        <p className={styles.des}>
                            <b>8. Real-Time Analytics: </b> <br />Gain insights on-the-go with real-time analytics. Make informed decisions, understand user behavior, and refine strategies with data that’s always a tap away.
                        </p>
                        <p className={styles.des}>
                            <b>9. Maintenance and Support: </b> <br />Navigate the app world with unwavering support. From troubleshooting to upgrades, our team is dedicated to ensuring your application is always at its peak performance.
                        </p>
                        <p className={styles.des}>
                            Embark on a mobile journey where each application is not just a product but a meticulous craft, tailored to offer stellar user experiences, drive engagement, and elevate your brand in the mobile domain.
                        </p>
                    </div>
                    
                </div>
            </div>
            </>
  )
}

export default MobileService