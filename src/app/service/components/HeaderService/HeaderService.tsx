import React from 'react'
import styles from './styles/header-service.module.scss'
import ScrollAppearEffect from '@/components/Effects/AppearEffect'
import CRMService from '../CRMService/CRMService'

const HeaderService = () => {
    return (
        <>
            <ScrollAppearEffect time={1} effectX={-30}>
                <div className={styles.headerService}>
                    <h1 className={styles.title}> OUR SERVICES WEST ADVISORY </h1>
                    <p className={styles.note}>
                        We are providing software implementation services, including software design and deployment services.
                    </p>
                </div>
            </ScrollAppearEffect>
            <CRMService />


        </>
    )
}

export default HeaderService