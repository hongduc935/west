
import React from 'react'
import styles from './styles/test-homepage.module.scss'
import Banner from './components/Banner/Banner'
import Content from './components/Content/Content'
import OurProcess from './components/OurProcess/OurProcess'
import WhyNeed from './components/WhyNeed/WhyNeed'
import Voucher from './components/Voucher/Voucher'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'WEST ADVISORY - Premier Zoho CRM Implementation Partner in Vietnam',
  description: 'WEST ADVISORY is the leading provider and consultant for CRM projects, specializing in Zoho CRM implementation in Vietnam. As a certified Zoho partner, we deliver top-notch CRM solutions to drive business success.',
  keywords: 'WEST ADVISORY, Zoho CRM, CRM implementation, CRM consulting, Vietnam, Zoho partner, business solutions, customer relationship management',
  
};

const HomePage = () => {
  return (
    <div className={styles.main}>
      <Banner />
      <Content />
      <WhyNeed />
      <Voucher />
      <OurProcess />
    </div>
  )
}

export default HomePage