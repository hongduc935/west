"use client"
import React from 'react'
import styles from './styles/list-video.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react'
// Import Swiper styles
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import SwiperCore, { Autoplay, Navigation } from 'swiper'
import ScrollAppearEffect from '@/components/Effects/AppearEffect'
const OurProcess = () => {

    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)
    SwiperCore.use([Autoplay])
    return (

        <div className={styles.listVideo}>
            <ScrollAppearEffect time={1.5} effectY={-30}>
                <p className={styles.title}>OUR PROCESS</p>
            </ScrollAppearEffect>
            <div className={styles.videos}>
                <div className={`${styles.banner} container`}>
                    <Swiper
                        breakpoints={{
                            420: {
                                width: 420,
                                slidesPerView: 1,
                            },
                            640: {
                                width: 640,
                                slidesPerView: 2,
                            },
                            768: {
                                width: 768,
                                slidesPerView: 3,
                            },
                            1080: {
                                width: 1080,
                                slidesPerView: 3.4,
                            },
                        }}
                        //   loop={true}
                        cssMode={true}
                        mousewheel={true}
                        keyboard={true}
                        loopPreventsSliding={true}
                        className="mySwiperProcess"
                        spaceBetween={30}
                        autoplay={{
                            delay: 3500,
                            disableOnInteraction: false,
                        }}
                        modules={[Autoplay, Navigation]}

                        navigation={{
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                        }}
                        onBeforeInit={(swiper: any) => {
                            swiper.params.navigation.prevEl = navigationPrevRef.current;
                            swiper.params.navigation.nextEl = navigationNextRef.current;
                        }}
                    >
                        <SwiperSlide>
                            <div className={styles.mainSlide} style={{ height: "310px" }}>
                                <div className={`${styles.mainSwiper} item-center`} style={{ backgroundImage: `url("/gathering.webp")`, height: "170px", backgroundSize: "cover", borderRadius: "30px", boxSizing: 'border-box', overflow: "hidden", backgroundPosition: "center" }}>
                                </div>
                                <p className={`${styles.title1}`} ><b>Requirement Gathering</b></p>
                                <p className={styles.content} >Understanding needs, objectives, and constraints for a project's success.</p>

                            </div>
                        </SwiperSlide>
                        <SwiperSlide>
                            <div className={styles.mainSlide} style={{ height: "310px" }}>
                                <div className={`${styles.mainSwiper} item-center`} style={{ backgroundImage: `url("/analysis.webp")`, height: "175px", backgroundSize: "cover", borderRadius: "30px", boxSizing: 'border-box', overflow: "hidden", backgroundPosition: "center" }}>
                                </div>
                                <p className={`${styles.title1}`} ><b>Analysis</b></p>
                                <p className={styles.content}>Analyzing, defining, and prioritizing essential project specifications and criteria systematically.</p>

                            </div>
                        </SwiperSlide>
                        <SwiperSlide>
                            <div className={styles.mainSlide} style={{ height: "310px" }}>
                                <div className={`${styles.mainSwiper} item-center`} style={{ backgroundImage: `url("/development.webp")`, height: "175px", backgroundSize: "cover", borderRadius: "30px", boxSizing: 'border-box', overflow: "hidden", backgroundPosition: "center" }}>
                                </div>
                                <p className={`${styles.title1} `} ><b>Implementation & Development</b></p>
                                <p className={styles.content} >Executing project plans, coding, and building software or systems efficiently.</p>

                            </div>
                        </SwiperSlide>
                        <SwiperSlide>
                            <div className={styles.mainSlide} style={{ height: "310px" }}>
                                <div className={`${styles.mainSwiper} item-center`} style={{ backgroundImage: `url("/qa.webp")`, height: "175px", backgroundSize: "cover", borderRadius: "30px", boxSizing: 'border-box', overflow: "hidden", backgroundPosition: "center" }}>
                                </div>
                                <p className={`${styles.title1} `}><b>QA & Testing</b></p>
                                <p className={styles.content} >
                                    Thoroughly verifying software functionality, identifying defects, and ensuring quality assurance.</p>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide>
                            <div className={styles.mainSlide} style={{ height: "310px" }}>
                                <div className={`${styles.mainSwiper} item-center`} style={{ backgroundImage: `url("/live.webp")`, height: "175px", backgroundSize: "cover", borderRadius: "30px", boxSizing: 'border-box', overflow: "hidden", backgroundPosition: "center" }}>
                                </div>
                                <p className={`${styles.title1}`} ><b>Go-Live & Support</b></p>
                                <p className={styles.content} >
                                    Transitioning to production and providing ongoing assistance for operational stability.</p>
                            </div>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
        </div>

    )
}

export default OurProcess