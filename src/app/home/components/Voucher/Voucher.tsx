"use client"
import styles from './styles/voucher.module.scss'
import React from 'react'
import 'swiper/css'
import 'swiper/css/pagination'
import ScrollAppearEffect from '@/components/Effects/AppearEffect'

const Voucher = () => {
    return (
        <div className={styles.voucher}>
            <ScrollAppearEffect time={1} effectY={30}>
                <p className={styles.title}>Why Choosing Us?</p>
            </ScrollAppearEffect>
            <div className={`${styles.section} item-btw w-100`} style={{ maxWidth: '100%' }}>
                <div className={styles.mainContent} style={{ width: '100%' }}>
                    <ScrollAppearEffect time={1.5} effectX={-100}>
                        <p className={styles.line1}>
                            <b>Service Quality Commitment</b> <br />
                            Our commitment is to crafting solutions that truly work for you. We're not just listeners; we're problem solvers. We provide dedicated support, whether it's before, during, or after each project.
                        </p>
                    </ScrollAppearEffect>
                    <ScrollAppearEffect time={1.5} effectX={100}>
                        <p className="tx tx1"><b>Constantly Innovating Through Research and Development</b><br />
                            West Advisory began its journey in software and it's a field we're deeply passionate about. We stay up-to-date with the latest trends from various industries. Our aim is to solve your puzzle and continually improve the quality of our services.</p>
                    </ScrollAppearEffect>
                    <ScrollAppearEffect time={1.5} effectX={-100}>
                        <p className="tx tx2"><b>Customer: Center of Everything We Do</b><br />
                            Our greatest assets is not just robust growth but also the trust and confidence from our customers. Our team of dedicated professionals is here to support your business every step of the way.</p>
                    </ScrollAppearEffect>
                </div>
            </div>
        </div>
    )
}

export default Voucher