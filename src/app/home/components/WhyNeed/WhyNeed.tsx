import React from 'react'
import styles from './styles/section4.module.scss'
import Image from '@/components/Image/Image'
import ScrollAppearEffect from '@/components/Effects/AppearEffect'
const WhyNeed = () => {
    return (

        <div className={`${styles.section4}`}>
            <h2 title='Tại sao cần sử dụng CRM trong doanh nghiệp' className={styles.title}>Why Business Need To Use CRM?</h2>
            <div className={`${styles.section} item-btw w-100`} style={{ marginBottom: '10%' }}>
                <div className={styles.image}>
                    <Image image='/crm.webp' contain='contain' />
                </div>
                <ScrollAppearEffect time={1.5} effectX={100}>
                    <div className={styles.mainContent}>
                        <p className={styles.line1}>
                            CRM systems are invaluable tools for businesses seeking to build stronger customer relationships, streamline operations, and gain a competitive advantage in today's highly competitive market. </p>
                        <p className="tx tx1">They enable companies to better understand, engage, and serve their customers, ultimately leading to improved profitability and growth.</p>
                        <p><a href='/Service'  target='_blank'> More Details</a></p>
                    </div>
                </ScrollAppearEffect>
            </div>
        </div >
    )
}

export default WhyNeed