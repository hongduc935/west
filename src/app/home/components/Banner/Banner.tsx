"use client"
import React from 'react'
import styles from './styles/banner.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import { Navigation ,Autoplay} from 'swiper'

const Banner = () => {

    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)
    return (
        <div className={`${styles.banner} `}>
            <>
                <Swiper
                    cssMode={true}
                    mousewheel={true}
                    keyboard={true}
                    autoplay={{
                        delay: 3500,
                        disableOnInteraction: false,
                    }}
                    modules={[Autoplay, Navigation]}
                    className="mySwiperTestBanner"
                    navigation={{
                        prevEl: navigationPrevRef.current,
                        nextEl: navigationNextRef.current,
                    }}
                    onBeforeInit={(swiper: any) => {
                        swiper.params.navigation.prevEl = navigationPrevRef.current;
                        swiper.params.navigation.nextEl = navigationNextRef.current;
                    }}
                >
                    <SwiperSlide>
                        <div className={`${styles.mainSwiper} item-center `} style={{ backgroundImage: `url(/banner-zoho-crm-implementation.webp)` }} />
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={`${styles.mainSwiper} item-center `} style={{ backgroundImage: `url("/mobile-appication-deveplopment.webp")` }} />
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className={`${styles.mainSwiper} item-center `} style={{ backgroundImage: `url("/banner-website.webp")` }} />
                    </SwiperSlide>
                </Swiper>
            </>
        </div>
    )
}

export default Banner