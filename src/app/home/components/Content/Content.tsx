import React from 'react'
import styles from './styles/content.module.scss'
import Image from '@/components/Image/Image'
import ScrollAppearEffect from '@/components/Effects/AppearEffect'

const Content = () => {
    return (
        <div className={`${styles.content} w-100`}>
            <ScrollAppearEffect time={0.5} effectY={20}>
                <h1 className={styles.title}>The elements that make up WEST</h1>
            </ScrollAppearEffect>
            <ScrollAppearEffect time={0.8} effectY={20}>
                <div className={`${styles.section} item-btw w-100`}>

                    <div className={styles.image}>
                        <Image image='/teamwork.webp' />
                    </div>
                    <div className={styles.mainContent}>
                        <p className="tx tx1">
                            Professional teamwork involves individuals working collaboratively toward common goals,
                            leveraging their diverse skills, knowledge, and perspectives.
                            It requires effective communication, mutual respect
                            and a shared commitment to achieving results.
                            Successful professional teams foster innovation,
                            problem-solving, and a supportive work environment to maximize productivity and success.
                        </p>
                    </div>
                </div>
            </ScrollAppearEffect>
            <ScrollAppearEffect time={1} effectY={20}>
                <div className={`${styles.section} ${styles.customPhonesection} item-btw w-100`}>
                    <div className={styles.mainContent2}>
                        <p className={styles.line1}>
                            CRM (Customer Relationship Management) solutions are software tools,
                            that help businesses manage and nurture customer interactions throughout the customer lifecycle.
                            They store customer data, track interactions,
                            and provide insights to improve customer service, sales, and marketing efforts
                            CRM solutions enhance customer relationships,
                            streamline processes, and drive business growth.
                        </p>
                    </div>
                    <div className={styles.image}>
                        <Image image='/solutions.webp' />
                    </div>
                </div>
            </ScrollAppearEffect>
            <ScrollAppearEffect time={1.2} effectY={20}>
                <div className={`${styles.section} item-btw w-100`} style={{ marginBottom: '10%' }}>
                    <div className={styles.image}>
                        <Image image='/deluge.webp' />
                    </div>
                    <div className={styles.mainContent}>
                        <p className={styles.line1}>
                            We have a team of programmers with many years of experience working with Zoho CRM.
                            We plan digital transformation processes for over 20 businesses, both domestic and international.
                            Additionally, we have a team of professional business analysts
                            to shape and optimize business processes for maximum efficiency.
                            Finally, We implemented the whole process of business using Zoho CRM
                            with custom function and integration with website or third party flatform.
                        </p>
                    </div>
                </div>
            </ScrollAppearEffect>
        </div>
    )
}

export default Content