import React from 'react'
import styles from './styles/product.module.scss'
import Banner from './comonents/Banner/Banner'

interface IItem{
  img:string
  name:string
}

const ListCRM :IItem[] = [
  {
    img:"https://play-lh.googleusercontent.com/E1vbfVVAv5VRjK7g1KZ6s4HCdbUrCg-dzgWs-T-lgw_M6U3tXqswRYRyWi8Px89nfj5P",
    name:"Zoho CRM"
  },
  {
    img:"https://is1-ssl.mzstatic.com/image/thumb/Purple126/v4/1f/07/33/1f073330-94df-0ac8-1650-a5bb118c858e/AppIcon-0-2x_U007euniversal-0-4-0-0-85-220.png/1200x630bb.png",
    name:"Zoho Book"
  },
  {
    img:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAvVBMVEX///8ibbTkJSfiAAAAZLAAYK8NZrEda7PkHyEUaLIAYa/jFBf7/f4JZbFXisJQhsA/fLtsl8jq8PdikcXkHR/86uri6vRLg77wl5fvkpOgu9qYtddym8pkksV3nstOhb/oUlPpXV7qZWbxnp/d5vLv9Pk5ebqxx+DjCg798fHra2zlLS/qYmPoUFGLq9LR3u34zs/ypqfmNDbA0uf74uLtfn/ui4z1v7/mQEL0tLWCpc/mP0D40dH2xMXrcXLtcXpXAAAIF0lEQVR4nO2dfUObOhTG0xegUKtWu3od63Su1voyOufmtt67ff+PdcH6AjkpEHLycto+fwoFfj7JSUhyAmM77bTTtmlv+nAVp7p6mO7ZfhYNWiQnYdQd+GHoD7pReJgsbD8Rqi7jWRQGQetVQeBHreTI9nNh6eij5+foXhVGw7HtZ0NR3A0FeM+MCf0auQgG6/gy+f6D7SdUVByV8WU10ktsP6OSJlWAqbr7hEvqgV8NmNbGQ7KIZ2tDDIc4I4pYF5As4kFtQKKIQsAg9P1C54YwIgQM0n7p/jJJlvuDtAtHHhEC+v5y+nJ0nMB+ADFEEGTShv0yf8JeHPE2kkIEDoYz0Mc+OuQbS0LtIgD0D0TPPulSdRH0ZPx98YkfACINF2sDMvaRJKIEIE3EfRlAioiSgPTqolQRXYmWiw0AaSFKF9GV6CA2BKSD2BiQCqICIA1EJUAKiIqA7iMqAwoRdTxpQyEAuu0iCqDLLiIBuusiGqCriIiAbiKiArqIiAzoHiI6oGuIx/iAbjUaWgBdclEToDsuagMUDE/5E6xLS+iYnz3CAxS4GJlfJKYVELoYfsC8eh1pBkxd5Gd3cC9fKe2AbFo0MQiRr18hA4CD4gRqcIJ8g3LpBxxzgC0/Rr5DqUwUUQ4w6F5W/wpNBhzkAVueyRWM5utgCniFfIsyWaiDGwcIVkzvADEFAQ+Q7+AcIHqQcQ1w4x1EB3QsioYbBjjZAarKch20ARhtNeD3H7f93sUftNtPwFrXM7RrrzQGC7+99yWnf+70e+32qHODdHsIaKAOlgGedtor9c9Rbg8BsUcUmgKmiF8Qbg/roGXAxzdAFETnAE/zgAiIBgDlgsxjEVAZ0bkoesoDKiIO9QcZZUAlRAio38FIGlCh0aDhoAKigXYQCbAhon4Hj+QA/1kP2AhxCGaa3XWwEaIVB8veJkodbIBoxcEyQOBgrz9SQdQPqFoHR7fXf+fNEQ0AgmxnaUDGbgDip5q3JwIoQjyvdXsA6DsK2NRFQoDNEAWAyIvKlAHb129HL6QRSTmYSdZFYg5mkgs35BzMBAvq+drr2XCw9H3wc6WDmSDixZrrwTWPBBzMBBDnj8LrOVcH6zmYCSB2fgrOEjhIoYiuBAsqPNW5IgoBxUV0JR6xD6Y09Dt42dJURFe64xA7X4vHnXOwuh3kxbnY+1U4asBB1SLae1d1Cw6xk59dNOCgchGtBGTsU7/wk/O3I3BrCvQoqgrYqyiiK30pDGx0Xv8p+vfeEACWLflt5mCq64KJ/c/Pf7bioBZAxn7nf9p7ns7Q76CgmdAEyNivXu6H86eibcPBSAqwJwHIfubj6VOTaMNBScCRBCBjecL+PWMJv4Eq+qjanhzgbyUHU93kwunokU093Q6yhJ8lLwW8VnSQsftcOB1dsGXIAyrRiMT/D0sB2T3XuexV92Q4/cldIe24zfjMG/RUzQVXDbrlqS8XI0VA9j1fCr4x7v+rIS/lgcvPqlg1XeyUtOdfy08XqEB4yw6LhPY9vMH18NZAPdyzWQ/b/5qIpUt+prW0P4MQS/OR5lN6f+2zTMrtoaSLhfbwB6PQK1Xs05Dol8q4+Bv0S0m4KIFYeLfoPL82GxgnNeZiwcK3sSgbozR6EN8VGpvXd3w7LmoJN7/WjNM46CIYLa3lIjfWVpgp1T8pA8dL0V+E7wqA/KC3jalRSRerXoXvSse8jbiodzijWERTC//jzzDgos66yBXRdv8vPMfKHD5Wo8E72BPMH9pZ7IXULvIOFqdl3uQcYt2CyjvY7pyuuaKVJXtyiKJwAxycr89pI+AibDSAg+VLhvQvflZF5F2UAzSRgoDsoiwgCcS8ixCwevml/mQuRBflHcxkoy5KRtSXRqOJg5n05/0iITZzMJONglqKCLMtsoLaHNBE/r0g30LWRRVAO8mxpeEGujhXypkxsZOJqou8pDNJDWzWgpI/2hhQVFDRww1m/mGTXGAD233g5ZA2S3a2gVhaUNe62DjV2TkX1yAqpKvbCDcN8vHPFR7AuYgqcFFxCxf3XVTeo8YAotzeJpyLCNu3WNmIrraLKFvwuOwi0jZKVrb0rOXi/BzpAWwglkbU+2yvr96oI5icaCjnXHz3+G3U/gumlxRkZWPPMkR8OVdQ8aXfRcH+s7YRDWxzvUNElnMRFV87RAQ5GFF3LkprGxCtR1R9H7J6luD7FpuGKPhGicl9oY24aPc7M1vwraAt+N7TFnyzawu+u2b+23lDzKvXk9aICtbWRyY/afUiDd+RfRFM5DnGurSUtCEKMpVwLiwtTYj6U7HqSwsiXORq8yPyGhBdcjATOqLAwc369LhrDmZCRXTPwUyIiG4CIiK6CoiGOHGqmSgKBdFdBzMhILrV0EMpI7rtYCZFRPcBRYgSbzwUAJVcpAGo4CIVQMYO+NGjei7SAWyISAmwESItwAaI1ABFiKXhhh6gpIsUAaVcpAko4SJVQMbO6iGCeYFwRgSQsRMeMZxN+XPGM3ASFQczAcTAWxbmqS+XHj9XT8jBTACx5fvLxcvRxTIEx4kBihCDMPJmw2UyPPQikEVCD5DtQcQnylSAjiQgE7m4XqSCzJtO+NZg0wDru0gWMG36a7lIGJCxCb8lukDdfcKAjMWRKHLmFSW2n1FRixa/8KYgP1hUX8N1xdHagBN61A1c6eiDJ4w4oTcc2342LB3FLc/Pb9gSpF24VrIxfE+axmdh1B34qQbdKDyJN6D+QY0frt7H8furhzHp9mGnnXaS0f9nKtSHK6irmQAAAABJRU5ErkJggg==",
    name:"Zoho Creator"
  }
]
const ListFE :IItem[] = [
  {
    img:"https://www.patterns.dev/img/reactjs/react-logo@3x.svg",
    name:"ReactJS"
  },
  {
    img:"https://play-lh.googleusercontent.com/5e7z5YCt7fplN4qndpYzpJjYmuzM2WSrfs35KxnEw-Ku1sClHRWHoIDSw3a3YS5WpGcI",
    name:"Flutter"
  },
  {
    img:"https://segwitz.com/wp-content/uploads/2021/06/vuejs-development-malaysia.jpeg",
    name:"VueJS"
  }
]
const ListBE :IItem[] = [
  {
    img:"https://media.geeksforgeeks.org/wp-content/cdn-uploads/20190902124355/ruby-programming-language.png",
    name:"Ruby"
  },
  {
    img:"https://www.thinktanker.io/wp-content/uploads/2021/03/share-nodejs-logo.png",
    name:"NodeJS"
  },
  {
    img:"https://www.freecodecamp.org/news/content/images/2021/10/golang.png",
    name:"Golang"
  }
]
const Technology = () => {
  return (
    <div className={`${styles.product} `}>
      <div className={styles.bannerProduct}>
          <Banner/>
      </div>
       <div className={`${styles.productDetails} container`}> 
        <p className={styles.title}>CRM Systems</p>
        <div className={`${styles.listItem} item-start`}>
            {
              ListCRM.map((value:IItem,index:number)=>{
                return <div className={index === 0 ?`${styles.item}` : `${styles.item} ${styles.mrItem}`} key={`crm${index}`}>
                          <div className={styles.image}>
                              <div className={styles.img} style={{backgroundImage:`url("${value.img}")`}}>

                              </div>
                          </div>
                          <div className={styles.name}>
                            <p className={styles.content}>{value.name}</p>
                          </div>
                    </div>
              })
            }
        </div>
      </div>
      <div className={`${styles.productDetails} container`}>
        <p className={styles.title}>Frontend Technologies</p>
        <div className={`${styles.listItem} item-start`}>
            {
              ListFE.map((value:IItem,index:number)=>{
                return <div className={index === 0 ?`${styles.item}` : `${styles.item} ${styles.mrItem}`} key={`fe${index}`}>
                          <div className={styles.image}>
                              <div className={styles.img} style={{backgroundImage:`url("${value.img}")`}}>

                              </div>
                          </div>
                          <div className={styles.name}>
                            <p className={styles.content}>{value.name}</p>
                          </div>
                    </div>
              })
            }
        </div>
      </div>
      <div className={`${styles.productDetails} container`}>
        <p className={styles.title}>Backend Technologies</p>
        <div className={`${styles.listItem} item-start`}>
            {
              ListBE.map((value:IItem,index:number)=>{
                return <div className={index === 0 ?`${styles.item}` : `${styles.item} ${styles.mrItem}`} key={`fe${index}`}>
                          <div className={styles.image}>
                              <div className={styles.img} style={{backgroundImage:`url("${value.img}")`}}>

                              </div>
                          </div>
                          <div className={styles.name}>
                            <p className={styles.content}>{value.name}</p>
                          </div>
                    </div>
              })
            }
        </div>
      </div>
    </div>
  )
}

export default Technology