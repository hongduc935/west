interface ISideBar{
    path:String
    title:String
    // icon:HTMLElement
}
const DataSidebarAdmin:ISideBar[]=[
    {
        path:"Home",
        title:"Home",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Teacher",
        title:"Teacher",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Student",
        title:"Teacher",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Account",
        title:"Account",
        // icon:<i class="fa-regular fa-book"/>
    },
]
const DataSidebarTeacher:ISideBar[]=[
    {
        path:"Topic",
        title:"Đề tài",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"GroupStudent",
        title:"Sinh viên",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"ViewReport",
        title:"Báo cáo",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Core",
        title:"Điểm số",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Profile",
        title:"Cá nhân",
        // icon:<i class="fa-regular fa-book"/>
    }
]
const DataSidebarLeader:ISideBar[]=[
    {
        path:"Topic",
        title:"Đề tài",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Teacher",
        title:"Hội đồng",
        // icon:<i class="fa-regular fa-book"/>
    },
    
    {
        path:"ViewReport",
        title:"Báo cáo",
        // icon:<i class="fa-regular fa-book"/>
    }  ,
    {
        path:"Core",
        title:"Điểm số ",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Profile",
        title:"Cá nhân",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Account",
        title:"Tài khoản",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Overview",
        title:"Tổng quan",
        // icon:<i class="fa-regular fa-book"/>
    }
]
const DataSidebarStudent:ISideBar[]=[

    {
        path:"Topic",
        title:"Đề tài ",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Group",
        title:"Hội đồng ",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"ViewReport",
        title:"Báo cáo",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Profile",
        title:"Cá nhân",
        // icon:<i class="fa-regular fa-book"/>
    },
    {
        path:"Subject",
        title:"Môn học",
        // icon:<i class="fa-regular fa-book"/>
    }
]

const SideBarDB={
    DataSidebarAdmin,
    DataSidebarLeader,
    DataSidebarStudent,
    DataSidebarTeacher
}
export default SideBarDB
