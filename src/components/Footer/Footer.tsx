import React from 'react'
import styles from './styles/footer.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faLinkedin, faYoutube } from '@fortawesome/free-brands-svg-icons'
const Footer = () => {
    return (
        <footer>
            <div className={`${styles.footer}`} id="contact">
                <div className={`${styles.mainFooter} item-btw`}>
                    <div className={`${styles.item1}  ${styles.item}`}>
                        <p className={styles.title}>WEST ADVISORY</p>
                        <p className={styles.note}>Whatsapp: +84 559 242 538</p>
                        <p className={styles.note}>E-mail: contact.westadvisory@gmail.com</p>
                        <p className={styles.note}>Address: 316A Đoan Hoang Minh Street , Phu Tan Ward, Ben Tre City</p>
                        <ul className={`${styles.icon} `}>
                        </ul>
                    
                    </div>
                    <div className={`${styles.item1}  ${styles.item}`}>
                        <p className={styles.title}>SOCIAL</p>
                        <ul className={`${styles.icon} `} >
                            <a href='https://www.facebook.com/profile.php?id=100095393118824' target="_blank">
                                <li> <FontAwesomeIcon icon={faFacebook} /></li></a>
                            <a href='https://www.linkedin.com/company/contact-west-advisory' target="_blank">
                                <li><FontAwesomeIcon icon={faLinkedin} /></li></a>
                            <a href='https://www.youtube.com/channel/UC2aa3Fwn9EiW-2uSKJ_RT2w' target="_blank">
                                <li><FontAwesomeIcon icon={faYoutube} /></li></a>
                        </ul>
                    </div>
                    <div className={`${styles.item1}  ${styles.item}`}>
                        <p className={styles.title}>WEST ADVISORY</p>
                        <p className={styles.shareContent}>Copyright © WEST ADVISORY 2023. All rights reserved</p>
                    </div>
                </div>
                <div className={`${styles.copyright} item-btw`}>
                    <div className={styles.source}>
                    
                    </div>

                </div>

            </div>
        </footer>
    )
}

export default Footer