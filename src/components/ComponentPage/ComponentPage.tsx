import React from 'react'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
const ComponentPage = (props: any) => {
  return (
    <>
      {props.isHiddenHeader === true && <Header />}
      {props.children}
      {props.SalesIQ}
      {props.isHiddenFooter === true && <Footer />}
      
    </>
  )
}

export default ComponentPage