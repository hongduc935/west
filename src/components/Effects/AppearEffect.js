'use client'
import React from 'react';
import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import { faBullseye } from '@fortawesome/free-solid-svg-icons/faBullseye';

const ScrollAppearEffect = (props) => {
    const [ref, inView] = useInView({
        triggerOnce: false,
    });
    if (props?.effectY !== undefined) {
        return (
            <div ref={ref}>
                <motion.div
                    initial={{ opacity: 0, y: props?.effectY }}
                    animate={{ opacity: inView ? 1 : 0, y: inView ? 0 : props?.effectY }}
                    transition={{ duration: props?.time }}
                > {props?.children}
                </motion.div>
            </div>
        );
    }
    else {
        return (
            <div ref={ref}>
                <motion.div
                    initial={{ opacity: 0, x: props?.effectX }}
                    animate={{ opacity: inView ? 1 : 0, x: inView ? 0 : props?.effectX }}
                    transition={{ duration: props?.time }}
                > {props?.children}
                </motion.div>
            </div>
        );
    }
};

export default ScrollAppearEffect;
