'use client'
import React, { useState } from "react";

const ReadMore = ({ children }) => {
    const text = children;
    const [isReadMore, setIsReadMore] = useState(true);
    const toggleReadMore = () => {
        console.log(isReadMore);
        setIsReadMore(!isReadMore);
    };
    return (
        <p className="text">
            {isReadMore ? text : `${text.toString().substring(0, 50)}`}
            <span onClick={toggleReadMore} className="read-or-hide">
                {isReadMore ? "...read more" : " show less"}
            </span>
        </p>
    );
};

export default ReadMore;