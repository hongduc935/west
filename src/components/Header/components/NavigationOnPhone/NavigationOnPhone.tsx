import Link from 'next/link'
import styles from './styles/navigation-on-phone.module.scss'
const NavigationOnPhone = (props: any) => {

  return (
    <div className={styles.mainNavigationPhone}>
      <ul className={styles.menuPhone}>
        <li className={styles.itemPhone} onClick={props?.handleCloseNavigationPhone} ><span><Link href={'home'}>Home </Link></span> </li>
        <li className={styles.itemPhone} onClick={props?.handleCloseNavigationPhone}><span><Link href={'about-us'}>About US </Link></span> </li>
        <li className={styles.itemPhone} onClick={props?.handleCloseNavigationPhone}><span><Link href={'service'}>Service </Link></span> </li>
        <li className={styles.itemPhone} onClick={props?.handleCloseNavigationPhone}><span><Link href='#contact'>Contact</Link></span> </li>
      </ul>
    </div>
  )
}

export default NavigationOnPhone