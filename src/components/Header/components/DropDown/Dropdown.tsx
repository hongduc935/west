import React, { useState } from "react";
import { serviceDropdown } from "./NavItems";
import Link from 'next/link';
import "./Dropdown.scss";
function Dropdown() {
    const [dropdown, setDropdown] = useState(false);

    return (
        <>
            <ul
                className={dropdown ? "services-submenu clicked" : "services-submenu"}
                onClick={() => setDropdown(!dropdown)}
            >
                {serviceDropdown.map((item) => {
                    return (
                        <li key={item.id} className="dropdownItem">
                            <Link
                                href={item.path}
                                className={item.cName}
                                target='_blank'
                                onClick={() => setDropdown(false)}
                            >
                                {item.title}
                            </Link>
                        </li>
                    );
                })}
            </ul>
        </>
    );
}

export default Dropdown;