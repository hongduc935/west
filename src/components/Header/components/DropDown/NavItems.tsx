export const navItems = [
    {
        id: 1,
        title: "Home",
        path: "./",
        cName: "nav-item",
    },
    {
        id: 2,
        title: "About US",
        path: "./about-us",
        cName: "nav-item",
    },
    {
        id: 3,
        title: "Services",
        path: "",
        cName: "nav-item",
    },
    {
        id: 4,
        title: "Contact Us",
        path: "./#contact",
        cName: "nav-item",
    },
    {
        id: 4,
        title: "Bim",
        path: "./bim",
        cName: "nav-item",
    },
];

export const serviceDropdown = [
    {
        id: 1,
        title: "Zoho CRM",
        path: "./service",
        cName: "submenu-item",
    },
    {
        id: 2,
        title: "Integration",
        path: "./integration",
        cName: "submenu-item",
    },
    {
        id: 3,
        title: "Web Application",
        path: "./web-service",
        cName: "submenu-item",
    },
    {
        id: 4,
        title: "Mobile Application",
        path: "./mobile-service",
        cName: "submenu-item",
    },
];