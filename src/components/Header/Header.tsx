"use client";
import React, { useState } from 'react'
import styles from './styles/header.module.scss'
import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faCaretDown, faXmark } from '@fortawesome/free-solid-svg-icons'
import NavigationOnPhone from './components/NavigationOnPhone/NavigationOnPhone'
import { navItems } from './components/DropDown/NavItems'
import Dropdown from './components/DropDown/Dropdown'

const Header = () => {
    let [showNavigation, setShowNavigation] = useState<Boolean>(false)
    const handleOpenNavigationPhone = () => {
        setShowNavigation(true)
    }
    const handleCloseNavigationPhone = () => {
        setShowNavigation(false)
    }
    const [dropdown, setDropdown] = useState(false);
    return (
        <div className={`${styles.header} w-100 container item-btw`}>
            <div className={`${styles.logo} item-center`}>
            </div>
            <div className={styles.menuList}>
                <ul className={`${styles.menu} item-center`}>
                    {navItems.map((item) => {
                        if (item.title === "Services") {
                            return (
                                <li
                                    key={item.id}
                                    className={item.cName}
                                    onMouseEnter={() => setDropdown(true)}
                                    onMouseLeave={() => setDropdown(false)}
                                >
                                    <Link href={item.path} target='_blank'>{item.title}</Link> &nbsp;
                                    <FontAwesomeIcon icon={faCaretDown} />
                                    {dropdown && <Dropdown />}
                                </li>
                            );
                        }
                        return (
                            <li key={item.id} className={item.cName}>
                                <Link href={item.path} target='_blank'>{item.title}</Link>
                            </li>
                        );
                    })}
                </ul>
            </div>
            <div className={styles.bar1}>
                <span className={styles.iconBar} onClick={showNavigation ? handleCloseNavigationPhone : handleOpenNavigationPhone}>
                    <FontAwesomeIcon icon={showNavigation ? faXmark : faBars} />
                </span>
            </div>
            {showNavigation ? <NavigationOnPhone handleCloseNavigationPhone={handleCloseNavigationPhone} /> : <></>}
        </div>
    )
}

export default Header