import React from 'react'
import Script from 'next/script';
const SaleIQ = () => {
  return (
    <Script 
    id="zoho-salesiq"
    strategy="afterInteractive" // Chạy sau khi trang đã có thể tương tác
    dangerouslySetInnerHTML={{
      __html: `
        var $zoho=$zoho || {};$zoho.salesiq = $zoho.salesiq || {
          widgetcode: "siqb2a79650f305d64e91bec89832096d6caf9530f4816847e176b06f0c62c066d1",
          values: {},
          ready: function(){}
        };
        var d=document;
        s=d.createElement("script");
        s.type="text/javascript";
        s.id="zsiqscript";
        s.defer=true;
        s.src="https://salesiq.zoho.com/widget";
        t=d.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(s,t);
      `
    }}
  />
  )
}

export default SaleIQ
